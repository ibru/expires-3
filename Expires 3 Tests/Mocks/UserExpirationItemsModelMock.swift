//
//  UserExpirationItemsModelMock.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 01/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
@testable import Expires_3

class MockExpirationItemsModel: UserExpirationItemsModelType {
    
    required init(userId: String) {
    }
    
    convenience init(items: [ExpirationItem]) {
        self.init(userId: "")
        self.items.value = items
    }
    
    var items: Dynamic<[ExpirationItem]?> = Dynamic(nil)
    
    var didAddItem: ((ExpirationItem) -> Void)?
    
    var didRemoveItem: ((ExpirationItem) -> Void)?
    
    var didChangeItem: ((ExpirationItem) -> Void)?
    
    var loadingError: Dynamic<Error?> = Dynamic(nil)
    
    func load(completion: (([ExpirationItem]?, Error?) -> Void)?) {
        completion?(self.items.value, nil)
    }
    
    func addItem(item: ExpirationItem) {
        self.didAddItem?(item)
    }
    
    func removeItem(item: ExpirationItem) {
        self.didRemoveItem?(item)
    }
    
    func changeItem(item: ExpirationItem) {
        self.didChangeItem?(item)
    }
    
    
}
