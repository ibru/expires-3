//
//  MockStoreInfoProvider.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
@testable import Expires_3

class MockStoreInfoProvider: StoreInfoProviderType {
    var priceChanged: (() -> Void)?
    
    var priceInfo: (price: Double, locale: Locale)?
    
}
