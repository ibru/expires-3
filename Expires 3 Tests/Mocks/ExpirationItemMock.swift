//
//  ExpirationItemMock.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 01/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
@testable import Expires_3

extension ExpirationItem {
    init(content: Int) {
        self.init(id: "\(content)", title: "\(content)", date: Date(timeIntervalSinceNow: TimeInterval(content)))
    }
}
