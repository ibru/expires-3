//
//  StoreInfoProviderTests.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import XCTest
@testable import Expires_3

class StoreInfoProviderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_priceInfoChanged() {
        var infoProvider: StoreInfoProviderType = StoreInfoProvider()
        
        let exp1 = expectation(description: "should call priceChanged 1")
        let exp2 = expectation(description: "should call priceChanged 2")
        
        infoProvider.priceChanged = {
            exp1.fulfill()
        }
        infoProvider.priceInfo = (1, Locale(identifier: "en_US"))
        
        infoProvider.priceChanged = {
            exp2.fulfill()
        }
        infoProvider.priceInfo = (2, Locale(identifier: "en_US"))
        
        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
}
