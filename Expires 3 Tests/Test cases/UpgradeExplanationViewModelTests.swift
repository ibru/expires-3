//
//  UpgradeExplanationViewModelTests.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import XCTest
@testable import Expires_3

class UpgradeExplanationViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_priceString_noPrice() {
        let info = MockStoreInfoProvider()
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.priceString.value, nil)
    }
    
    func test_priceString_hasPriceNotFree() {
        let info = MockStoreInfoProvider()
        info.priceInfo = (2.99, Locale(identifier: "en_US"))
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.priceString.value, "$2.99")
    }
    
    func test_priceString_hasPriceFree() {
        let info = MockStoreInfoProvider()
        info.priceInfo = (0, Locale(identifier: "en_US"))
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.priceString.value, "Free")
    }
    
    func test_priceString_priceChanged() {
        let info = StoreInfoProvider()
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        info.priceInfo = (2.99, Locale(identifier: "en_US"))
        
        XCTAssertEqual(viewModel.priceString.value, "$2.99")
        
        let exp = expectation(description: "Price should change")
        
        viewModel.priceString.bind { (string) in
            exp.fulfill()
        }
        info.priceInfo = (1.99, Locale(identifier: "en_US"))
        
        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_upgradeButtonTitle_noValue() {
        let info = StoreInfoProvider()
        let viewModel = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.upgradeButtonTitle.value, "Upgrade")
    }
    
    func test_upgradeButtonTitle_free() {
        let info = StoreInfoProvider()
        info.priceInfo = (0, Locale(identifier: "en_US"))
        let viewModel = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.upgradeButtonTitle.value, "Upgrade for Free")
    }
    
    func test_upgradeButtonTitle_priceValue() {
        let info = StoreInfoProvider()
        info.priceInfo = (2.99, Locale(identifier: "en_US"))
        let viewModel = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertEqual(viewModel.upgradeButtonTitle.value, "Upgrade for $2.99")
    }
    
    func test_upgradeButtonTitle_bindingCallbackFires() {
        let info = StoreInfoProvider()
        let viewModel = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        let exp = expectation(description: "Binding callback fires")
        viewModel.upgradeButtonTitle.bind { (title) in
            exp.fulfill()
            XCTAssertEqual(title, "Upgrade for $2.99")
            XCTAssertEqual(viewModel.upgradeButtonTitle.value, "Upgrade for $2.99")
        }
        info.priceInfo = (2.99, Locale(identifier: "en_US"))
        
        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    
    func test_isUpgradeAvailable_isNotAvailable() {
        let info = StoreInfoProvider()
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertFalse(viewModel.isUpgradeAvailable.value)
    }
    
    func test_isUpgradeAvailable_isAvailable() {
        let info = StoreInfoProvider()
        info.priceInfo = (1, Locale(identifier: "en_US"))
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertTrue(viewModel.isUpgradeAvailable.value)
    }
    
    func test_isUpgradeAvailable_bindingChangeFires() {
        let info = StoreInfoProvider()
        let viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        XCTAssertFalse(viewModel.isUpgradeAvailable.value)
        
        let exp = expectation(description: "Should fire callback")
        viewModel.isUpgradeAvailable.bind { (isAvailable) in
            exp.fulfill()
            XCTAssertTrue(viewModel.isUpgradeAvailable.value)
            XCTAssertTrue(isAvailable)
        }
        info.priceInfo = (1, Locale(identifier: "asdf"))
        
        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    
    var mockDelegate: UpgradeExplanationViewModelDelegate?
    
    func test_purchaseUpgrade_cannotPurchaseIfPurchaseNotAvailable() {
        let info = StoreInfoProvider()
        var viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        
        class MockDelegate: UpgradeExplanationViewModelDelegate {
            func viewModelWantsToPurchaseUpgrade(_ viewModel: UpgradeExplanationViewModelType) {
                XCTFail("shold not call delegate method")
            }
        }
        mockDelegate = MockDelegate()
        viewModel.delegate = mockDelegate
        viewModel.purchaseUpgrade()
    }
    
    func test_purchaseUpgrade_canPurchaseIfPurchaseAvailable() {
        let info = StoreInfoProvider()
        var viewModel: UpgradeExplanationViewModelType = UpgradeExplanationViewModel(storeInfoProvider: info)
        info.priceInfo = (1, Locale(identifier: "abcd"))
        
        class MockDelegate: UpgradeExplanationViewModelDelegate {
            let exp: XCTestExpectation
            init(exp: XCTestExpectation) { self.exp = exp }
            func viewModelWantsToPurchaseUpgrade(_ viewModel: UpgradeExplanationViewModelType) {
                exp.fulfill()
            }
        }
        mockDelegate = MockDelegate(exp: expectation(description: "Delegate method should be called"))
        viewModel.delegate = mockDelegate
        viewModel.purchaseUpgrade()
        
        waitForExpectations(timeout: 0.1) { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
}
/*
class MockDelegate: UpgradeExplanationViewModelDelegate {
    func viewModelWantsToPurchaseUpgrade(_ viewModel: UpgradeExplanationViewModelType) {
        xctassert
    }
    
    let exp: XCTestExpectation
    init(exp: XCTestExpectation) { self.exp = exp }
}*/
