//
//  UserItemsLimitedViewModelTests.swift
//  Expires 3 Tests
//
//  Created by Jiri Urbasek on 01/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import XCTest
@testable import Expires_3

class UserItemsLimitedViewModelTests: XCTestCase {
    
    var mockModel: UserExpirationItemsModelType!
    var testedModel: LimitedUserItemsViewModelType!
    
    override func setUp() {
        super.setUp()
        
        let items = [ExpirationItem(id: "1", title: "1", date: Date(timeIntervalSinceNow: 0)),
                     ExpirationItem(id: "2", title: "2", date: Date(timeIntervalSinceNow: 1)),
                     ExpirationItem(id: "3", title: "3", date: Date(timeIntervalSinceNow: 2)),
                     ExpirationItem(id: "4", title: "4", date: Date(timeIntervalSinceNow: 3)),
                     ExpirationItem(id: "5", title: "5", date: Date(timeIntervalSinceNow: 4))]
        mockModel = MockExpirationItemsModel(items: items)
        testedModel = UserItemsLimitedViewModel(model: mockModel, limit: 10)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_itemsCount() {
        testedModel.load()
        XCTAssertEqual(testedModel.itemsCount.value, 5)
    }
    
    func test_didAddItem_canAdd() {
        let items = [ExpirationItem(content: 1),ExpirationItem(content: 2), ExpirationItem(content: 3)]
        let mockModel = MockExpirationItemsModel(items: items)
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: 10)
        testedViewModel.load()
        
        let exp = expectation(description: "Did add item")
        testedViewModel.didAddItem = { idx in
            exp.fulfill()
        }
        mockModel.addItem(item: ExpirationItem(content: 4))
        
        waitForExpectations(timeout: 2, handler: { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        })
    }
    
    func test_didAddItem_cannotAdd() {
        let items = [ExpirationItem(content: 1),ExpirationItem(content: 2), ExpirationItem(content: 3)]
        let mockModel = MockExpirationItemsModel(items: items)
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: 2)
        
        testedViewModel.load()
        
        testedViewModel.didAddItem = { idx in
            XCTFail("Should not add item")
        }
        mockModel.addItem(item: ExpirationItem(content: 5))
    }
    
    func test_didRemoveItem_canRemove() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3)]
        let mockModel = MockExpirationItemsModel(items: items)
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: 5)
        
        testedViewModel.load()
        
        let exp = expectation(description: "Did remove item")
        let idxToRemove = 1
        let itemToRemove = items[idxToRemove]
        
        testedViewModel.didRemoveItem = { idx in
            XCTAssertEqual(idx, idxToRemove)
            exp.fulfill()
        }
        mockModel.removeItem(item: itemToRemove)
        
        waitForExpectations(timeout: 1, handler: { (error) in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        })
    }
    
    func test_didRemoveItem_cannotRemove() {
        let items = [ExpirationItem(content: 1),ExpirationItem(content: 2), ExpirationItem(content: 3)]
        let mockModel = MockExpirationItemsModel(items: items)
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: 5)
        
        testedViewModel.load()
        
        testedViewModel.didRemoveItem = { item in
            XCTFail("Should not removes item")
        }
        mockModel.removeItem(item: ExpirationItem(content: 999))
    }
    
    func test_didRemoveItem_removeWithOverLimit() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: 3)
        
        testedViewModel.load()
        
        let expRemove = expectation(description: "Did remove item")
        let expAdd = expectation(description: "Did add item")
        let idxToRemove = 1
        
        testedViewModel.didRemoveItem = { idx in
            XCTAssertEqual(idx, idxToRemove)
            expRemove.fulfill()
        }
        testedViewModel.didAddItem = { idx in
            expAdd.fulfill()
            
            XCTAssertNotNil(testedViewModel.itemsLimit)
            XCTAssertEqual(idx, testedViewModel.itemsLimit! - 1)
            let itemModel = testedViewModel.itemModel(at: idx)
            
            XCTAssertEqual(itemModel?.item.firebaseId, "4")
        }
        
        mockModel.removeItem(item: items[idxToRemove])
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_updateLimit_removeLimit() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        var limit: Int? = 3
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        
        testedViewModel.load()
        
        XCTAssertEqual(limit, testedViewModel.itemsLimit)
        XCTAssertEqual(limit, testedViewModel.itemsCount.value)
        
        limit = nil
        let exp = expectation(description: "Reload items")
        testedViewModel.didChangeAllItems = {
            exp.fulfill()
        }
        
        testedViewModel.updateLimit(limit: limit)
        
        XCTAssertEqual(limit, testedViewModel.itemsLimit)
        XCTAssertEqual(items.count, testedViewModel.itemsCount.value)
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_updateLimit_addLimit() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        var limit: Int? = nil
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        
        testedViewModel.load()
        
        XCTAssertEqual(limit, testedViewModel.itemsLimit)
        XCTAssertEqual(items.count, testedViewModel.itemsCount.value)
        
        limit = 2
        let exp = expectation(description: "Reload items")
        testedViewModel.didChangeAllItems = {
            exp.fulfill()
        }
        
        testedViewModel.updateLimit(limit: limit)
        
        XCTAssertEqual(limit, testedViewModel.itemsLimit)
        XCTAssertEqual(limit, testedViewModel.itemsCount.value)
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_updateLimit_identicalLimit_num() {
        // TODO: test that if we chage limit to same value, no updates get triggered, test number value
    }
    
    func test_updateLimit_identicalLimit_nil() {
        // TODO: test that if we chage limit to same value, no updates get triggered, test nil value
    }
    
    func test_hasItemsOverLimit_notWhenLoadingData() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let limit: Int? = items.count + 1
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        testedViewModel.load()
        
        XCTAssertFalse(testedViewModel.hasItemsOverLimit.value)
    }
    
    func test_hasItemsOverLimit_whenLoadingData() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let limit: Int? = items.count - 1
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        testedViewModel.load()
        
        XCTAssert(testedViewModel.hasItemsOverLimit.value)
    }
    
    func test_hasItemsOverLimit_afterAddingItem() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let limit: Int? = items.count
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        testedViewModel.load()
        
        XCTAssertFalse(testedViewModel.hasItemsOverLimit.value)
        mockModel.addItem(item: ExpirationItem(content: 4))
        XCTAssert(testedViewModel.hasItemsOverLimit.value)
    }
    
    func test_hasItemsOverLimit_notAfterRemovingItem() {
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let limit: Int? = items.count - 1
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        testedViewModel.load()
        
        XCTAssert(testedViewModel.hasItemsOverLimit.value)
        mockModel.removeItem(item: items.last!)
        XCTAssertFalse(testedViewModel.hasItemsOverLimit.value)
    }
    
    func test_delegateMethodCallendWhenNeedsUpdateLimit() {
        
        class MockDelegate: LimitedUserItemsViewModelInteractionDelegate {
            let exp: XCTestExpectation
            init(exp: XCTestExpectation) { self.exp = exp }
            
            func viewModelWantsToIncreaseItemsLimit(_ viewModel: LimitedUserItemsViewModelType) {
                exp.fulfill()
            }
        }
        
        let items = [ExpirationItem(content: 1), ExpirationItem(content: 2), ExpirationItem(content: 3), ExpirationItem(content: 4)]
        let mockModel = MockExpirationItemsModel(items: items)
        let limit: Int? = items.count
        let testedViewModel = UserItemsLimitedViewModel(model: mockModel, limit: limit)
        testedViewModel.load()
        
        let exp = expectation(description: "Call delegate method")
        testedViewModel.limitDelegate = MockDelegate(exp: exp)
        
        testedViewModel.askToUpdateLimit()
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
}
