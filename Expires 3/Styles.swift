//
//  Styles.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 26/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func rgb(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
    }
    
    // MARK: Color palete
    
    static var expViolet: UIColor { // this color looks like to be edited to look good in translucent environment
        return UIColor.rgb(47, 29, 180)
    }
    
    static var expViolet2: UIColor { // real violet
        return UIColor(red: 42.0/255.0, green: 30.0/255.0, blue: 109.0/255.0, alpha: 1) // #2a1e6d
    }
    
    static var expLightViolet: UIColor {
        return UIColor.rgb(134, 123, 202)
    }
    
    static var expLightLightViolet: UIColor {
        return UIColor.rgb(234, 232, 241) // #eae8f1
    }
    
    static var expRed: UIColor {
        return UIColor.rgb(241, 83, 88)
    }
    
    static var expRedButton: UIColor {
        return UIColor.rgb(228, 41, 50)
    }
    
    static var expRedButtonSelected: UIColor {
        return expRedButton.withAlphaComponent(0.6)
    }
    
    static var expLightRed: UIColor {
        return UIColor.rgb(241, 140, 143)
    }
    
    static var expLightLightRed: UIColor {
        return UIColor.rgb(251, 221, 221)
    }
    
    static var expWhite: UIColor {
        return UIColor.white
    }
}

var dateFormatter: DateFormatter = {
    
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    
    if let dateFormat = formatter.dateFormat {
        formatter.dateFormat = "EEE, \(dateFormat)"
    }
    return formatter
}()

func applyDefaultStyle() {
    UINavigationBar.appearance().tintColor = UIColor.expWhite
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.expWhite]
    UINavigationBar.appearance().barTintColor = .expViolet2
    UINavigationBar.appearance().isTranslucent = false
}

extension UIViewController {
    func hideBackButtonTitleOnNextScreen() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func makeNavigationBarTransparent() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
}

