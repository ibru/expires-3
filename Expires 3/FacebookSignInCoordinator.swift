//
//  FacebookSignInCoordinator.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

protocol ServiceSignInCoordinatorDelegate: class {
    func coordinatorDidSignIn(_ coordinator: Coordinator)
    func coordinatorDidCancel(_ coordinator: Coordinator)
}

class FacebookSignInCoordinator: Coordinator {
    fileprivate var navigationController: UINavigationController
    internal var coordinators: [Coordinator] = []
    fileprivate var viewModel: FacebookSignInViewModel
    
    weak var delegate: ServiceSignInCoordinatorDelegate?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.viewModel = FacebookSignInViewModelImpl()
    }
    
    func start() {
        let currentUser = AppState.shared.currentUser
        if currentUser?.isAnonymous ?? false {
            viewModel.userToLink = currentUser
        }
        viewModel.signIn(from: navigationController.visibleViewController!)
        
        viewModel.signedInUser.bind { (user) in
            guard user != nil else { return }
            self.delegate?.coordinatorDidSignIn(self)
        }
        viewModel.signInError.bind { (error) in
            guard let error = error else { return }
            self.navigationController.visibleViewController?.present(error: error)
        }
    }
}
