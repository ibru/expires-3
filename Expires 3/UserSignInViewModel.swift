//
//  UserSignInViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol UserSignInViewModelType {
    var email: String { get set }
    var password: String { get set }
    var fullName: String { get set }
    var userToLink: DatabaseUser? { get set }
    
    var canSignIn: Dynamic<Bool> { get set }
    var canRegister: Dynamic<Bool> { get set }
    var signedInUser: Dynamic<DatabaseUser?> { get set }
    var signInError: Dynamic<Error?> { get set }
    
    func signIn()
    func register()
}

class UserSignInViewModel: UserSignInViewModelType {
    
    var userToLink: DatabaseUser?
        
    var email: String = "" {
        didSet {
            canSignIn.value = !email.isEmpty && !password.isEmpty
            canRegister.value = !email.isEmpty && !password.isEmpty
        }
    }
    var password: String = "" {
        didSet {
            canSignIn.value = !email.isEmpty && !password.isEmpty
            canRegister.value = !email.isEmpty && !password.isEmpty
        }
    }
    var fullName: String = ""
    
    var canSignIn = Dynamic<Bool>(false)
    
    var canRegister = Dynamic<Bool>(false)
    
    var signedInUser = Dynamic<DatabaseUser?>(nil)
    
    var signInError = Dynamic<Error?>(nil)
    
    func signIn() {
        guard canSignIn.value else { return }
        
        signIn(userToLink: userToLink)
    }
    
    func register() {
        guard canRegister.value else { return }
        
        if let linkUser = userToLink as? User { // if we have user to link, we actually want to do sign in instead
            canRegister.value = false
            let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            linkUser.link(with: credential)  { (result, error) in
                self.canRegister.value = true
                
                let err = error as NSError?
                if err?.domain == AuthErrorDomain, err?.code == AuthErrorCode.emailAlreadyInUse.rawValue {
                    self.signIn(userToLink: nil)
                } else {
                    self.didFinishSignIn(user: result?.user, error: error)
                }
            }
        } else {
            canRegister.value = false
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                self.canRegister.value = true
                self.didFinishSignIn(user: result?.user, error: error)
            }
        }
    }
    
    private func didFinishSignIn(user: DatabaseUser?, error: Error?) {
        signedInUser.value = user
        signInError.value = error
    }
    
    private func signIn(userToLink: DatabaseUser?) {
        if let linkUser = userToLink as? User {
            let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            
            canSignIn.value = false
            linkUser.link(with: credential)  { (result, error) in
                self.canSignIn.value = true
                
                let err = error as NSError?
                if err?.domain == AuthErrorDomain, err?.code == AuthErrorCode.emailAlreadyInUse.rawValue {
                    self.signIn(userToLink: nil)
                } else {
                    self.didFinishSignIn(user: result?.user, error: error)
                }
            }
        }
        else {
            canSignIn.value = false
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                self.canSignIn.value = true
                self.didFinishSignIn(user: result?.user, error: error)
            }
        }
    }
}
