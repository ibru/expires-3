//
//  NotificationsManager.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/02/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import UserNotifications


protocol ExpirationItemsProvider {
    var expirationItems: [ExpirationItem]? { get }
}

final class NotificationsManager: NSObject {

    var currentNotificationSettings: UNNotificationSettings?
    
    
    static let shared: NotificationsManager = NotificationsManager()
    
    
    func requestNotificationAuthorization(completion: @escaping (_ granted: Bool, _ error: Error?) -> Void) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .badge, .alert], completionHandler: { (granted, error) in
                DispatchQueue.main.async {
                    completion(granted, error)
                }
        })
    }
    
    func getCurrentNotificationSettings(callback: ((UNNotificationSettings) -> Void)? = nil) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            self.currentNotificationSettings = settings
            callback?(settings)
        }
    }
    
    class func scheduleNotifications(forItem item: ExpirationItem, allItems: [ExpirationItem], notificationHour: Int = 8, notificationMinute: Int = 0, errorCallback: ((_ notificationIdentifier: String, _ error: Error) -> Void)? = nil) {
        guard let reminders = item.reminders else { return }
        
        for reminderInterval in reminders {
            if reminderInterval >= 0 {
                let expirationDate = item.expirationDate.rounded(toHour: notificationHour, minute: notificationMinute)
                let notifIntervalSinceNow = expirationDate.timeIntervalSinceNow - TimeInterval(reminderInterval)
                
                guard notifIntervalSinceNow > 0 else { return }
                
                let notifDate = Date(timeIntervalSinceNow: notifIntervalSinceNow)
                let badgeNum = numberOfItemsPassedAlertDate(fromItems: allItems, after: notifDate)
                
                let daysRemaining = reminderInterval / (60*60*24)
                let notifIdentifier = item.firebaseId + "-\(reminderInterval)-\(Int(notifIntervalSinceNow))"
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notifIntervalSinceNow, repeats: false)
                
                let content = UNMutableNotificationContent()
                content.title = String(format: item.title)
                content.body = String(format: NSLocalizedString("%@ is expiring in %d days on %@", comment: ""), item.title, daysRemaining, dateFormatter.string(from: expirationDate))
                content.badge = badgeNum as NSNumber
                
                let info: [String: Any] = ["itemId": item.firebaseId, "itemDict": item.dictValue]
                content.userInfo = info
                
                if let notes = item.notes {
                    content.subtitle = notes
                }
                
                let request = UNNotificationRequest(identifier: notifIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                    if let error = error {
                        errorCallback?(notifIdentifier, error)
                    }
                })
            }
        }
    }
    
    class func testScheduleNotification(for item: ExpirationItem, fireAfter interval: TimeInterval) {
        
        let expirationDate = item.expirationDate
        let daysRemaining = 3
        let notifIdentifier = item.firebaseId + "-\(666)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = String(format: item.title)
        content.body = String(format: NSLocalizedString("%@ is expiring in %d days on %@", comment: ""), item.title, daysRemaining, dateFormatter.string(from: expirationDate))
        
        let info: [String: Any] = ["itemId": item.firebaseId, "itemDict": item.dictValue]
        content.userInfo = info
        
        if let notes = item.notes {
            content.subtitle = notes
        }
        
        let request = UNNotificationRequest(identifier: notifIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            if let error = error {
                //errorCallback?(notifIdentifier, error)
                print("Error setting notification: \(error)")
            }
        })
    }
    
    class func removeNotifications(forItem item: ExpirationItem, completion: @escaping () -> Void) {
//        guard let reminders = item.reminders else { return }
//
//        var identifiers = [String]()
//
//        for reminderInterval in reminders {
//            if reminderInterval >= 0 {
//                let notifIdentifier = item.firebaseId + "-\(reminderInterval)"
//                identifiers.append(notifIdentifier)
//            }
//        }
//        if identifiers.count > 0 {
//            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
//        }
//
//
//
        
        let requestIdentifierPrefix = item.firebaseId
        UNUserNotificationCenter.current().getPendingNotificationRequests { requests in
            let requestsIDsToCancel = requests.map { $0.identifier }.filter { $0.hasPrefix(requestIdentifierPrefix) }
            
            if requestsIDsToCancel.count > 0 {
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: requestsIDsToCancel)
            }
            completion()
        }
    }
    
    class func rescheduleNotifications(itemsProvider: ExpirationItemsProvider, notificationHour: Int, notificationMinute: Int, errorCallback: ((_ notificationIdentifier: String, _ error: Error) -> Void)? = nil) {
        // TODO: Idealy we should check if provider has notifications and if not wait until it has or dont save changed hour, minute into userDefaults. Also check if there were not any errors when rescheduling notifications
        
        let allItems = itemsProvider.expirationItems ?? []
        for item in allItems {
            rescheduleNotifications(forItem: item, allItems: allItems, notificationHour: notificationHour, notificationMinute: notificationMinute)
        }
    }
    
    class func rescheduleNotifications(forItem item: ExpirationItem, allItems: [ExpirationItem], notificationHour: Int, notificationMinute: Int, errorCallback: ((_ notificationIdentifier: String, _ error: Error) -> Void)? = nil) {
        removeNotifications(forItem: item) {
            
            self.scheduleNotifications(forItem: item, allItems: allItems, notificationHour: notificationHour, notificationMinute: notificationMinute, errorCallback: errorCallback)
        }
    }
    
    class func numberOfItemsPassedAlertDate(fromItems items: [ExpirationItem], after date: Date = Date()) -> Int {
        var count = 0
        for item in items {
            var alertDate = item.expirationDate
            
            if let greatestReminder = item.reminders?.sorted(by: > ).first {
                alertDate = alertDate.addingTimeInterval(TimeInterval(-greatestReminder))
            }
            let daysRemining = alertDate.roundedDaysRemining(untilDate: date)
            
            if daysRemining <= 0 {
                count += 1
            }
        }
        return count
    }
    
    class func numberOfItemsPassedExpirationDate(fromItems items: [ExpirationItem], after date: Date = Date()) -> Int {
        var count = 0
        for item in items {
            let daysRemining = item.expirationDate.roundedDaysRemining(untilDate: date)
            
            if daysRemining <= 0 {
                count += 1
            }
        }
        return count
    }
    
    class func clearDeliveredNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
}
