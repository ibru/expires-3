//
//  AlertController.swift
//  Tradewise
//
//  Created by Jiri Urbasek on 13/10/16.
//  Copyright © 2016 TRDR. All rights reserved.
//

import UIKit

public protocol AlertableViewController {
    func dismissAlert(completion: (() -> Void)?)
}

extension UIViewController: AlertableViewController {
    public func dismissAlert(completion: (() -> Void)? = nil) {
        if view.window == AlertPresenter.shared.alertWindow {
            
            if let animator = AlertPresenter.shared.alertAnimator, let animatableController = self as? AnimatableController  {
                animator.animateDismissal(ofController: animatableController, completion: {
                    AlertPresenter.shared.alertWindow.isHidden = true
                    completion?()
                })
            }
            else {
                AlertPresenter.shared.alertWindow.isHidden = true
                completion?()
            }
        }
    }
}

public class AlertPresenter: NSObject {
    
    public let alertAnimator: AlertAnimator? = DefaultAlertAnimator()
    
    public static var shared: AlertPresenter = AlertPresenter()
    
    internal lazy var alertWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindow.Level.alert
        window.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        return window
    }()
    
    public class var alertStoryboard: UIStoryboard {
        return UIStoryboard(name: "Alerts", bundle: nil)
    }

    public func present<T: UIViewController>(alertController viewController: T) where T:AlertableViewController {
        alertWindow.rootViewController = viewController
        
        if let animator = alertAnimator, let animatableController = viewController as? AnimatableController {
            animator.animatePresentation(ofController: animatableController, completion: nil)
        }
        alertWindow.isHidden = false
        
    }
}

public protocol AnimatableController {
    var view: UIView! { get }
    var containerView: UIView! { get }
}

public protocol AlertAnimator {
    func animatePresentation(ofController controller: AnimatableController, completion: (() -> Void)?)
    func animateDismissal(ofController controller: AnimatableController, completion: (() -> Void)?)
}

public class DefaultAlertAnimator : AlertAnimator {
    
    let duration: TimeInterval = 0.4
    
    public func animatePresentation(ofController controller: AnimatableController, completion: (() -> Void)?) {
        controller.view.alpha = 0
        controller.containerView.center.y -= controller.view.bounds.size.height
        
        // Animate the presented view to it's final position
        UIView.animate(withDuration: self.duration, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
            controller.view.alpha = 1
            controller.containerView.center.y += controller.view.bounds.size.height
            }, completion: {(completed: Bool) -> Void in
                completion?()
        })
    }
    
public func animateDismissal(ofController controller: AnimatableController, completion: (() -> Void)?) {
    
        
        UIView.animate(withDuration: self.duration, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
            controller.containerView.center.y -= controller.view.bounds.size.height
            controller.view.alpha = 0
            
            }, completion: {(completed: Bool) -> Void in
                completion?()
        })
    }
}
