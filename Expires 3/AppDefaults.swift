//
//  AppDefaults.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 22/05/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation

private let kUserDefaultsAlertTimeHourKey = "jf858fngas85mg82lkfyts83kfi82fdi9ki93"
private let kUserDefaultsAlertTimeMinuteKey = "kdhjfy895g9igikgus892hjfyqikfu9qo3jfg8"

extension UserDefaults {
    
    static var appDefaults: UserDefaults {
        return UserDefaults.standard
    }
    
    func registerDefaults() {
        register(defaults: [kUserDefaultsAlertTimeHourKey: 8, kUserDefaultsAlertTimeMinuteKey: 0])
    }
    
    var alertTimeHour: Int {
        get { return integer(forKey: kUserDefaultsAlertTimeHourKey) }
        set { set(newValue, forKey: kUserDefaultsAlertTimeHourKey) }
    }
    var alertTimeMinute: Int {
        get { return integer(forKey: kUserDefaultsAlertTimeMinuteKey) }
        set { set(newValue, forKey: kUserDefaultsAlertTimeMinuteKey) }
    }
}
