//
//  DatePeriods.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/02/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class DatePeriods {
    
    private struct Keys {
        static let format = "format"
        static let argument = "argument"
        static let value = "value"
    }

    let datePeriods: [[String: AnyObject]]
    
    init() {
        if let path = Bundle.main.path(forResource: "DatePeriods", ofType: "plist"), let array = NSArray.init(contentsOfFile: path) as? [[String: AnyObject]] {
            datePeriods = array
        }
        else {
            datePeriods = [[String: AnyObject]]()
        }
    }
    
    var count: Int {
        return datePeriods.count
    }
    
    func period(atIndex index: Int) -> Int? {
        guard index < count, let periodInterval = datePeriods[index][Keys.value] as? Int else {
            return nil
        }
        return periodInterval
    }
    
    func localizedTitle(atIndex index: Int) -> String? {
        guard let interval = period(atIndex: index) else {
            return nil
        }
        return localizedTitle(forPeriod: interval)
    }
    
    func localizedTitle(forPeriod period: Int) -> String? {
        for periodDict in datePeriods {
            guard let periodValue = periodDict[Keys.value] as? Int, let periodFormat = periodDict[Keys.format] as? String, let periodArgument = periodDict[Keys.argument] as? Int else {
                continue
            }
            if periodValue == period {
                return String(format: NSLocalizedString(periodFormat, comment: ""), arguments: [periodArgument])
            }
        }
        return nil
    }
}
