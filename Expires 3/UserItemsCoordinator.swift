//
//  UserItemsCoordinator.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 06/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import UserNotifications
import StoreKit
import MessageUI


class ProductInfo {
    var product: SKProduct? {
        didSet {
            if let product = product {
                infoProvider.priceInfo = (product.price.doubleValue, product.priceLocale)
            } else {
                infoProvider.priceInfo = nil
            }
        }
    }
    private(set) var infoProvider: StoreInfoProviderType
    
    init(infoProvider: StoreInfoProviderType) { self.infoProvider = infoProvider }
    
}

class UserItemsCoordinator: Coordinator {
    
    var coordinators: [Coordinator] = []
    
    fileprivate var window: UIWindow
    fileprivate var splitViewController: UISplitViewController
    
    fileprivate var ignoreDisabledNotifications = false
    
    fileprivate var itemsViewController: ItemsViewController?
    fileprivate var itemsViewModel: (LimitedUserItemsViewModelType & ExpirationItemsProvider)?
    
    fileprivate var productInfo = ProductInfo(infoProvider: StoreInfoProvider())
    
    fileprivate var mailComposerDelegate: MailComposerDelegate?
    
    fileprivate lazy var proUpgradeExplanationViewController: ProUpgradeExplanationViewController = {
        let viewModel = UpgradeExplanationViewModel(storeInfoProvider: productInfo.infoProvider)
        viewModel.delegate = self
        let viewController = ProUpgradeExplanationViewController.create(from: splitViewController.storyboard!, with: viewModel)
        return viewController
    }()
    
    private func newSettingsViewController() -> SettingsViewController {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        viewController.delegate = self
        return viewController
    }
    
    fileprivate var iapManager: IAPManager
    
    init(window: UIWindow, rootController: UISplitViewController) {
        self.window = window
        splitViewController = rootController
        
        iapManager = IAPManager()
        iapManager.delegate = self
    }
    
    func start() {
        func newSettedUpItemsViewModel() -> LimitedUserItemsViewModelType & ExpirationItemsProvider {
            var itemsViewModel = createViewModel()
            itemsViewModel.delegate = self
            itemsViewModel.limitDelegate = self
            return itemsViewModel as! ExpirationItemsProvider & LimitedUserItemsViewModelType
        }
        
        func resetItemsViewController(shouldLoad: Bool) {
            self.itemsViewModel = newSettedUpItemsViewModel()
            self.itemsViewController?.viewModel = self.itemsViewModel
            
            if shouldLoad && (self.itemsViewController?.isViewLoaded ?? false) {
                self.itemsViewModel?.load()
            }
        }
        
        NotificationCenter.default.addObserver(forName: .enabledAppFeaturesChanged, object: nil, queue: OperationQueue.main) { notif in
            self.itemsViewModel?.updateLimit(limit: self.itemsLimit)

            // dismiss upgrade explanation screen if visible
            if self.proUpgradeExplanationViewController.viewIfLoaded?.window != nil {
                self.proUpgradeExplanationViewController.dismiss(animated: true, completion: nil)
            }
        }
        NotificationCenter.default.addObserver(forName: .authStateChanged, object: nil, queue: OperationQueue.main) { _ in
            resetItemsViewController(shouldLoad: true)
        }
        
        let navController = splitViewController.viewControllers.first as? UINavigationController
        itemsViewController = navController?.viewControllers.first as? ItemsViewController
        itemsViewController?.hideBackButtonTitleOnNextScreen()
        
        resetItemsViewController(shouldLoad: false)
        
        splitViewController.delegate = itemsViewController
        splitViewController.preferredDisplayMode = .allVisible
        
        window.rootViewController = splitViewController
        window.makeKeyAndVisible()
        
        NotificationsManager.clearDeliveredNotifications()
    }
    
    func showUpgradeToPROScreen(presentingViewController: UIViewController) {
        productInfo.product = nil
        
        let navController = UINavigationController(rootViewController: proUpgradeExplanationViewController)
        proUpgradeExplanationViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done) { _ in
            self.productInfo.product = nil
            presentingViewController.dismiss(animated: true, completion: nil)
        }
        presentingViewController.present(navController, animated: true, completion: nil)
        
        try? self.iapManager.requestProductData(productIdentifiers: [IAPProduct.expiresPro.rawValue])
    }
}

extension UserItemsCoordinator {
    fileprivate var itemsLimit: Int? {
        return AppState.shared.enabledFeatures.contains(.unlimitedItemsCount) ? nil : K.itemsLimitCount
    }
    
    fileprivate func createViewModel() -> LimitedUserItemsViewModelType {
        var model: UserExpirationItemsModelType?
        if let userId = AppState.shared.currentUser?.id {
            model = AppState.shared.modelFactory.userExpirationItemsModel(userId: userId)
        }
        return UserItemsLimitedViewModel(model: model, limit: self.itemsLimit)
    }
}

extension UserItemsCoordinator: UserItemsViewModelInteractionDelegate, LimitedUserItemsViewModelInteractionDelegate {
    
    func viewModel(_ viewModel: UserItemsViewModelType, wantsShowDetailFor itemModel: EditExpirationItemViewModel) {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "ItemDetailViewController") as! ItemDetailViewController
        
        var viewModel = itemModel
        viewModel.delegate = viewController
        viewController.viewModel = viewModel
        
        let navController = UINavigationController(rootViewController: viewController)
        splitViewController.showDetailViewController(navController, sender: nil)
    }
    
    func viewModelWantsShowSettings(_ viewModel: UserItemsViewModelType) {
        let navController = UINavigationController(rootViewController: newSettingsViewController())
        
        splitViewController.present(navController, animated: true)
    }
    
    func viewModelWantsShowUserAccountSettings(_ viewModel: UserItemsViewModelType) {
        let settingsViewController = newSettingsViewController()
        let navController = UINavigationController(rootViewController: settingsViewController)
        
        splitViewController.present(navController, animated: true) {
            settingsViewController.presentAccountScreen()
        }
    }
    
    func viewModel(_ viewModel: UserItemsViewModelType, wantsShowNotificationsEnablingPrompt needsToGoToSystemSettings: Bool) {
        
        doOnMainThreadAfter(delay: 0.5) {
            
            guard !self.ignoreDisabledNotifications else { return }
            
            if needsToGoToSystemSettings {
                let viewController = AlertPresenter.alertStoryboard.instantiateViewController(withIdentifier: "AlertNotificationsAuthorizationViewController") as! AlertNotificationsAuthorizationViewController
                viewController.alertTitle = NSLocalizedString("Notifications are disabled", comment: "")
                viewController.alertSubtitle = NSLocalizedString("Some of your expiration items contain reminder notifications. Your notifications are currently disabled, you will not receive expiration reminders until you enable notifications in system Settings.", comment: "")
                viewController.actionButtonTitle = NSLocalizedString("Go to Settings", comment: "")
                viewController.doActionHandler = { completion in
                    
                    completion(true)
                    
                    if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                        UIApplication.shared.open(settingsUrl)
                    }
                    self.ignoreDisabledNotifications = true
                }
                viewController.notNowHandler = { completion in
                    completion(true)
                    self.ignoreDisabledNotifications = true
                }
                
                AlertPresenter.shared.present(alertController: viewController)
            } else {
                let viewController = AlertPresenter.alertStoryboard.instantiateViewController(withIdentifier: "AlertNotificationsAuthorizationViewController") as! AlertNotificationsAuthorizationViewController
                viewController.alertTitle = NSLocalizedString("Notifications not enabled", comment: "")
                viewController.alertSubtitle = NSLocalizedString("Some of your expiration items contain reminder notifications. To be able to receive expiration reminders you will need to enable notifications in system Settings.", comment: "")
                viewController.actionButtonTitle = NSLocalizedString("Enable notifications", comment: "")
                viewController.doActionHandler = { completion in
                    NotificationsManager.shared.requestNotificationAuthorization { (granted, error) in
                        NotificationsManager.shared.getCurrentNotificationSettings()
                        
                        completion(error == nil)
                        
                        if !granted {
                            self.ignoreDisabledNotifications = true
                        }
                    }
                }
                viewController.notNowHandler = { completion in
                    completion(true)
                    self.ignoreDisabledNotifications = true
                }
                
                AlertPresenter.shared.present(alertController: viewController)
            }
        }
    }
    
    func viewModelWantsAddNewItem(_ viewModel: UserItemsViewModelType) {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "ExpirationTypesViewController") as! ExpirationTypesViewController
        let navController = UINavigationController(rootViewController: viewController)
        
        splitViewController.present(navController, animated: true) 
    }
    
    func viewModelWantsToIncreaseItemsLimit(_ viewModel: LimitedUserItemsViewModelType) {
        showUpgradeToPROScreen(presentingViewController: splitViewController)
    }
    
    func viewModel(_ viewModel: UserItemsViewModelType, didUpdateItems items: [ExpirationItem]) {
        // refresh badge num
        let badgeNum = NotificationsManager.numberOfItemsPassedAlertDate(fromItems: items)
        UIApplication.shared.applicationIconBadgeNumber = badgeNum
        
        guard let itemsProvider = itemsViewModel else { return }
        
        NotificationsManager.rescheduleNotifications(itemsProvider: itemsProvider, notificationHour: UserDefaults.appDefaults.alertTimeHour, notificationMinute: UserDefaults.appDefaults.alertTimeMinute)
    }
}

extension UserItemsCoordinator: UpgradeExplanationViewModelDelegate {
    func viewModelWantsToPurchaseUpgrade(_ viewModel: UpgradeExplanationViewModelType) {
        guard let product = productInfo.product else { return }
        iapManager.buyProduct(product: product)
    }
}

extension UserItemsCoordinator: SettingsViewControllerDelegate {
    func controllerWantsRateOnAppStore(_ controller: SettingsViewController) {
        SKStoreReviewController.requestReview()
    }
    
    func controllerWantsSendFeedback(_ controller: SettingsViewController) {
        guard MFMailComposeViewController.canSendMail() else {
            let alertController = UIAlertController(title: NSLocalizedString("Cannot send feedback", comment: ""), message: NSLocalizedString("Mail services are not available on this device", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
            controller.present(alertController, animated: true)
            return  
        }
        let composeVC = MFMailComposeViewController()
        mailComposerDelegate = MailComposerDelegate(presentingController: controller)
        composeVC.mailComposeDelegate = mailComposerDelegate
        composeVC.setToRecipients([K.feedbackEmailAddress])
        composeVC.setSubject(K.feedbackEmailSubject)
        composeVC.setMessageBody("\n\n\n\n" + deviceInfo(), isHTML: false)
        
        controller.present(composeVC, animated: true)
    }
    
    func controller(_ controller: SettingsViewController, didUpdateAlertTime hour: Int, minute: Int) {
        guard let itemsProvider = itemsViewModel else { return }
        
        NotificationsManager.rescheduleNotifications(itemsProvider: itemsProvider, notificationHour: hour, notificationMinute: minute)
    }
    
    func controllerWantsUpgradeToPRO(_ controller: SettingsViewController) {
        showUpgradeToPROScreen(presentingViewController: controller)
    }
    
    func controllerWantsRestorePurchases(_ controller: SettingsViewController) {
        iapManager.restoreTransactions()
    }
}

extension UserItemsCoordinator: IAPManagerDelegate {
    func managerNoProductsAvailableToBuy(manager: IAPManager) {
        let alertController = UIAlertController(title: "Upgrade not available", message: "Unable to find any products to purchase on AppStore", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        splitViewController.present(alertController, animated: true, completion: nil)
    }
    
    func manager(_ manager: IAPManager, didReceiveResponseForProduct availableProduct: String, product: SKProduct) {
        productInfo.product = product
    }
    
}

extension UserItemsCoordinator: IAPTransactionsControllerDelegate {
    func controller(_ controller: IAPTransactionsController, didStartPurchaing productId: String) {
        // we dont care
    }
    
    func controller(_ controller: IAPTransactionsController, didCompletePurchasing productId: String, with transaction: SKPaymentTransaction) {
        // we dont care, AppState catches the completion notification
    }
    
    func controller(_ controller: IAPTransactionsController, didRestore transactions: [SKPaymentTransaction]) {
        // we dont care, AppState catches the completion notification
    }
    
    func controller(_ controller: IAPTransactionsController, didReceiveError error: Error?) {
        if let error = error {
            (window.visibleViewController() ?? splitViewController).present(error: error)
        }
    }
    
    
}

class MailComposerDelegate: NSObject, MFMailComposeViewControllerDelegate {
    private let presentingViewController: UIViewController
    init(presentingController: UIViewController) { presentingViewController = presentingController }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        presentingViewController.dismiss(animated: true, completion: nil)
        
        let resultTitle: String
        if result == .cancelled {
            resultTitle = "Canceled"
        }
        else if result == .sent {
            resultTitle = "Sent"
        }
        else if result == .saved {
            resultTitle = "Saved"
        }
        else if result == .failed {
            let alertController = UIAlertController(title: NSLocalizedString("Sending failed", comment: "") , message: NSLocalizedString("Unable to send email from your device.", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            presentingViewController.present(alertController, animated: true, completion: nil)
        }
        else {
            resultTitle = "unknown"
        }
        //analyticsLogEvent(.FeedbackEmailWasFinished, args: ["result": resultTitle])
    }
}

private func deviceInfo() -> String {
    let shortBundleVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "N/A"
    let bundleVersion: String = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String ?? "-"
    let appName: String = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "-"
    
    var deviceInfo =
        "\(appName) v\(shortBundleVersion)(\(bundleVersion))\n" +
    "iOS \(UIDevice.current.systemVersion)\n"
    
    var systemInfo = utsname()
    uname(&systemInfo)
    let mirror = Mirror(reflecting: systemInfo.machine)
    let identifier = mirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
    deviceInfo += "device type: \(identifier)\n"
    
    return deviceInfo
}

extension UIWindow {

    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }

    static func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        if let navigationController = vc as? UINavigationController,
            let visibleController = navigationController.visibleViewController  {
            return UIWindow.getVisibleViewControllerFrom( vc: visibleController )
        } else if let tabBarController = vc as? UITabBarController,
            let selectedTabController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: selectedTabController )
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
}
