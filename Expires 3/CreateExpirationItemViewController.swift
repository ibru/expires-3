//
//  CreateExpirationItemViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import Firebase


class CreateExpirationItemViewController: EditExpirationItemBaseViewController {
    
    /// move to super class
    
    private var createItemBarButton: UIBarButtonItem!
    ///
    
    @IBOutlet weak var iconEventType: UIImageView!
    
    lazy var viewModel: CreateExpirationItemViewModelType = CreateExpirationItemViewModel()
    
    override func loadView() {
        super.loadView()
        
        title = NSLocalizedString("Add event", comment: "")
        
        createItemBarButton = UIBarButtonItem(image: UIImage(named: "icon_done"), style: .plain, target: self, action: #selector(createItemButtonTouched(sender:)))
        navigationItem.rightBarButtonItem = createItemBarButton
        
        itemTypeTextField.delegate = self
        itemTypeTextField.tintColor = UIColor.expViolet
        itemTypeTextField.textColor = UIColor.expViolet
        
        iconEventType.tintColor = UIColor.expViolet
        
        tableView.separatorStyle = .singleLine
        tableView.separatorEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .extraLight))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        viewModel.itemType = itemType
        
        viewModel.canCreateItem.bindAndFire { (canCreate) in
            self.createItemBarButton.isEnabled = canCreate
        }
        viewModel.itemCreationError.bindAndFire { (error) in
            if let error = error {
                self.createItemBarButton.isEnabled = true
                self.present(error: error)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        itemTypeTextField.text = itemType
        
        if itemType == nil {
            itemTypeTextField.becomeFirstResponder()
        }
        else {
            doOnMainThreadAfter(delay: 0.5) {
                self.showDatePicker()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        appAnalytics.log(event: .createExpirationItemScreenShown)
    }
    
    // MARK: EditExpirationItemBaseViewController
    
    override func didChangeDate(date: Date) {
        super.didChangeDate(date: date)
        
        viewModel.expirationDate = date
    }
    
    override func didChangeReminderTimes(times: [Int]) {
        super.didChangeReminderTimes(times: times)
        
        viewModel.reminderIntervals = times
    }
    
    override func didChangeType(type: String) {
        super.didChangeType(type: type)
        
        viewModel.itemType = type
    }
    
    override func didChangeNotes(notes: String?) {
        super.didChangeNotes(notes: notes)
        
        viewModel.notes = notes
    }
    
    // MARK: UITableViewDataSource, UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.backgroundColor = UIColor.expLightViolet.withAlphaComponent(0.1)
        
        if let iconTextCell = cell as? IconTextCell {
            iconTextCell.titleLabel.textColor = UIColor.expViolet
            
            if indexPath.row == dateRowIdx {
                iconTextCell.iconView.image = UIImage(named: "icon_expiration_date_violet")
            }
            else if indexPath.row == alertsRowIdx {
                iconTextCell.iconView.image = UIImage(named: "icon_advance_notice_violet")
            }
        }
        if let datePickerCell = cell as? DatePickerCell {
            datePickerCell.pickerTextColor = UIColor.expViolet
        }
        if let notesCell = cell as? IconTextViewCell {
            notesCell.textView.textColor = UIColor.expViolet
            notesCell.textViewPlaceholderLabel.textColor = UIColor.expViolet
        }
        
        return cell
    }
    
    // MARK: Actions
    
    @objc func createItemButtonTouched(sender: UIBarButtonItem) {
        appAnalytics.log(event: .createExpirationItemScreenCreateItemButtonTouched)
        viewModel.createItem()
        createItemBarButton.isEnabled = false
    }
    
    // MARK: EditExpirationItemBaseViewController
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        super.textFieldDidEndEditing(textField)
        
        navigationItem.setRightBarButton(createItemBarButton, animated: true)
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        super.textViewDidEndEditing(textView)
        
        navigationItem.setRightBarButton(createItemBarButton, animated: true)
    }
    
    override func prepareControllerForPresentation(viewController: UIViewController) {
        super.prepareControllerForPresentation(viewController: viewController)
        
        if let reminderTimeController = viewController as? ReminderTimeViewController {
            reminderTimeController.delegate = self
            reminderTimeController.style = .addWizard
        }
    }
}

extension CreateExpirationItemViewController: CreateExpirationItemViewModelDelegate {
    func viewModel(_ viewModel: CreateExpirationItemViewModel, didCreateItem item: ExpirationItem) {
        createItemBarButton.isEnabled = true
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

