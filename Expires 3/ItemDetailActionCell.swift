//
//  ItemDetailActionCell.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 04/04/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class ItemDetailActionCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 2
        
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        
        textLabel?.backgroundColor = UIColor.expRedButton
        textLabel?.textColor = UIColor.expWhite
        textLabel?.layer.cornerRadius = 3
        textLabel?.layer.masksToBounds = true
        
        self.selectedBackgroundView = nil;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = contentView.frame.insetBy(dx: 4, dy: 4)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        textLabel?.backgroundColor = selected ? UIColor.expRedButtonSelected : UIColor.expRedButton
    }
    
    /*override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        textLabel?.backgroundColor = highlighted ? UIColor.expRedButtonSelected : UIColor.expRedButton
    }*/

}
