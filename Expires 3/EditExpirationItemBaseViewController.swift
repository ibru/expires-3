//
//  EditExpirationItemBaseViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 31/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class EditExpirationItemBaseViewController: UIViewController {
    
    @IBOutlet weak var itemTypeTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    internal var notesTextView: UITextView?
    internal var notesTextViewPlaceholderLabel: UILabel?
    
    var itemType: String?
    var expirationDate: Date?
    var reminderIntervals: [Int]?
    var notes: String?
    
    internal var datePickerIndexPath: IndexPath?
    
    internal var isDatePickerVisible: Bool {
        return datePickerIndexPath != nil
    }
    
    internal var datePeriods = DatePeriods()
    
    
    override func loadView() {
        super.loadView()
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemTypeTextField.delegate = self
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        itemTypeTextField.text = itemType
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }

        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    // MARK: Actions
    
    @objc func datePickerValueChanged(datePicker: UIDatePicker) {
        didChangeDate(date: datePicker.date)
    }
    
    @objc func saveTypeEditingButtonTouched() {
        itemTypeTextField.resignFirstResponder()
        
        guard let type = itemTypeTextField.text, !type.isEmpty else {
            return
        }
        didChangeType(type: type)
    }
    
    @objc func cancelTypeEditingButtonTouched() {
        itemTypeTextField.resignFirstResponder()
        itemTypeTextField.text = itemType
    }
    
    @objc func saveNotesEditingButtonTouched() {
        notesTextView?.resignFirstResponder()
        
        if let textView = notesTextView {
            didChangeNotes(notes: textView.text)
        }
    }
    
    @objc func cancelNotesEditingButtonTouched() {
        notesTextView?.resignFirstResponder()
        notesTextView?.text = notes
        notesTextView.map { textViewDidChange($0) } // to hide placehoder label if needed
    }
    
    // MARK: Internal
    
    internal var dateRowIdx: Int {
        return 0
    }
    internal var datePickerRowIdx: Int? {
        return isDatePickerVisible ? 1 : nil
    }
    internal var alertsRowIdx: Int {
        return isDatePickerVisible ? 2 : 1
    }
    internal var notesRowIdx: Int {
        return isDatePickerVisible ? 3 : 2
    }
    
    internal func showDatePicker() {
        
        guard !isDatePickerVisible else {
            return
        }
        
        UILabel.appearance(whenContainedInInstancesOf: [UIDatePicker.self]).textColor = UIColor.white
        
        datePickerIndexPath = IndexPath(row: 1, section: 0)
        tableView.insertRows(at: [datePickerIndexPath!], with: .top)
    }
    
    internal func hideDatePicker() {
        guard isDatePickerVisible else {
            return
        }
        
        if let pickerPath = datePickerIndexPath {
            datePickerIndexPath = nil
            tableView.deleteRows(at: [pickerPath], with: .top)
        }
    }
    
    internal func didChangeType(type: String) {
        itemType = type
    }
    
    internal func didChangeNotes(notes: String?) {
        self.notes = notes
    }
    
    internal func didChangeDate(date: Date) {
        expirationDate = date
        tableView.reloadRows(at: [IndexPath(row: dateRowIdx, section: 0)], with: .automatic)
    }
    
    internal func didChangeReminderTimes(times: [Int]) {
        reminderIntervals = times
        tableView.reloadRows(at: [IndexPath(row: alertsRowIdx, section: 0)], with: .automatic)
    }
    
    internal func prepareControllerForPresentation(viewController: UIViewController) {
        
        if let reminderTimeController = viewController as? ReminderTimeViewController {
            reminderTimeController.delegate = self
            reminderTimeController.style = .itemDetail
            reminderTimeController.selectedReminderTimes = reminderIntervals
        }
    }
}

extension EditExpirationItemBaseViewController: UITableViewDataSource, UITableViewDelegate {
    
    private struct CellIdentifier {
        static let iconText = "Icon Text Cell"
        static let iconTextView = "Icon Text View Cell"
        static let datePicker = "Date Picker Cell"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isDatePickerVisible ? 4 : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case dateRowIdx:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.iconText, for: indexPath) as! IconTextCell
            cell.titleLabel.text = NSLocalizedString("Expiration date", comment: "")
            cell.iconView.image = UIImage(named: "icon_expiration_date_white")
            
            if let date = expirationDate {
                cell.titleLabel.text = dateFormatter.string(from: date)
            }
            
            return cell
            
        case datePickerRowIdx ?? 99999:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.datePicker, for: indexPath) as! DatePickerCell
            
            if (cell.datePicker.actions(forTarget: self, forControlEvent: .valueChanged)?.count ?? 0) == 0 {
                cell.datePicker.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
            }
            
            if let date = expirationDate {
                cell.datePicker.date = date
            }
            
            return cell
            
        case alertsRowIdx:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.iconText, for: indexPath) as! IconTextCell
            cell.titleLabel.text = NSLocalizedString("Advance notice", comment: "")
            cell.iconView.image = UIImage(named: "icon_advance_notice_white")
            cell.accessoryType = .disclosureIndicator
            
            if let interval = reminderIntervals?.first {
                cell.titleLabel.text = datePeriods.localizedTitle(forPeriod: interval)
            }
            
            return cell
        case notesRowIdx:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.iconTextView, for: indexPath) as! IconTextViewCell
            cell.textView.delegate = self
            cell.textView.text = notes
            notesTextView = cell.textView
            notesTextViewPlaceholderLabel = cell.textViewPlaceholderLabel
            notesTextViewPlaceholderLabel?.text = NSLocalizedString("Add note", comment: "")
            
            textViewDidChange(cell.textView) // to hide placehoder label if needed
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.iconText, for: indexPath)  as! IconTextCell
            cell.iconView.image = nil
            cell.titleLabel.text = nil
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case dateRowIdx, alertsRowIdx:
            return 64
            
        case datePickerRowIdx ?? 99999:
            return 200
            
        case notesRowIdx:
            return 132
            
        default:
            return 64
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == dateRowIdx {
            if isDatePickerVisible {
                hideDatePicker()
            }
            else {
                showDatePicker()
            }
            doOnMainThreadAfter(delay: 0.3) {
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        else if indexPath.row == alertsRowIdx {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ReminderTimeViewController") as! ReminderTimeViewController
            prepareControllerForPresentation(viewController: viewController)
            
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension EditExpirationItemBaseViewController: UITextFieldDelegate, UITextViewDelegate {
    
    // UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTypeEditingButtonTouched))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTypeEditingButtonTouched))
        
        navigationItem.setLeftBarButton(cancelButton, animated: true)
        navigationItem.setRightBarButton(saveButton, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        navigationItem.setLeftBarButton(nil, animated: true)
        navigationItem.setRightBarButton(nil, animated: true)
    }
    
    // UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveNotesEditingButtonTouched))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelNotesEditingButtonTouched))
        
        navigationItem.setLeftBarButton(cancelButton, animated: true)
        navigationItem.setRightBarButton(saveButton, animated: true)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        navigationItem.setLeftBarButton(nil, animated: true)
        navigationItem.setRightBarButton(nil, animated: true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        notesTextViewPlaceholderLabel?.isHidden = !textView.text.isEmpty
    }
}

extension EditExpirationItemBaseViewController: ReminderTimeViewControllerDelegate {
    
    func controller(_ controller: ReminderTimeViewController, didSetReminderTime reminderTime: Int) {
        didChangeReminderTimes(times: [reminderTime])
    }
    
}

