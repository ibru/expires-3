//
//  SignInCoordinator.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

@objc protocol SignInCoordinatorDelegate: class {
    func coordinatorDidAuthenticate(coordinator: SignInCoordinator)
    func coordinator(coordinator: SignInCoordinator, didCancelWith error: Error?)
}

@objc class SignInCoordinator: NSObject, Coordinator {
    
    fileprivate var navigationController: UINavigationController
    fileprivate var singInViewController: UserSignInViewController
    
    var coordinators: [Coordinator] = []
    
    @objc weak var delegate: SignInCoordinatorDelegate?
    
    @objc init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        let viewModel = UserSignInViewModel()
        let currentUser = AppState.shared.currentUser
        
        if currentUser?.isAnonymous ?? false {
            viewModel.userToLink = currentUser
        }
        
        singInViewController = UserSignInViewController.create(with: Storyboard.main, viewModel: viewModel)
    }
    
    @objc func start() {
        singInViewController.delegate = self
        
        let navController = UINavigationController(rootViewController: singInViewController)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(stop))
        singInViewController.navigationItem.leftBarButtonItem = doneButton
        
        navigationController.topViewController?.present(navController, animated: true, completion: nil)
    }
    
    @objc func stop() {
        navigationController.topViewController?.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func doFacebookSignIn() {
        let fbCoordinator = FacebookSignInCoordinator(navigationController: navigationController)
        fbCoordinator.delegate = self
        fbCoordinator.start()
        addCoordinator(fbCoordinator)
    }
    
    fileprivate func doTwitterSignIn() {
        // not yet implemented
    }
    
    fileprivate func doAppleSignIn() {
        // uncomment when ready to test "Sign in with Apple" feature
        let appleCoordinator = AppleSignInCoordinator(navigationController: navigationController)
        appleCoordinator.delegate = self
        appleCoordinator.start()
        addCoordinator(appleCoordinator)
    }
}

extension SignInCoordinator: UserSignInViewControllerDellegate {
    func viewController(_ viewController: UserSignInViewController, didChooseSignInMethod method: UserSignInViewController.SignInMethod) {
        switch method {
        case .twitter: doTwitterSignIn()
        case .facebook: doFacebookSignIn()
        case .apple: doAppleSignIn()
        }
    }
    
    func viewControllerWantsResetEmail() {
        let viewModel = UserResetPasswordViewModel()
        viewModel.delegate = self
        let viewController = UserResetPasswordViewController.create(with: Storyboard.main, viewModel: viewModel)
        
        singInViewController.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func viewControllerDidAuthenticate(_ viewController: UserSignInViewController) {
        delegate?.coordinatorDidAuthenticate(coordinator: self)
    }
}


extension SignInCoordinator: ServiceSignInCoordinatorDelegate {
    func coordinatorDidCancel(_ coordinator: Coordinator) {
        //delegate?.coordinator(coordinator: self, didCancelWith: nil) // uncomment if we want to hide whole sign in screen
        removeCoordinator(coordinator)
    }
    
    func coordinatorDidSignIn(_ coordinator: Coordinator) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // give it a little time for state change to go into AppState
            let provider = AppState.shared.currentUser?.signInService
            appAnalytics.log(event: .signedInUserAccount, params: ["provider": "\(provider)"])
        }
        
        delegate?.coordinatorDidAuthenticate(coordinator: self)
        removeCoordinator(coordinator)
    }
}

extension SignInCoordinator: UserResetPasswordViewModelDelegate {
    func viewModel(viewModel: UserResetPasswordViewModel, didResetEmailWith success: Bool, error: Error?) {
        if success {
            let alertController = UIAlertController(title: NSLocalizedString("Password reset", comment: ""), message: NSLocalizedString("Check your inbox for instructions how to renew your pasword.", comment: ""), preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
                self.singInViewController.navigationController?.popViewController(animated: true)
            }))
            singInViewController.present(alertController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: NSLocalizedString("Password reset failed", comment: ""), message: error?.localizedDescription ?? "Unknown error.", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
            }))
            singInViewController.present(alertController, animated: true, completion: nil)
        }
    }
    
    
}

