//
//  AppleSignInCoordinator.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 11/12/2019.
//  Copyright © 2019 Jiri Urbasek. All rights reserved.
//

import UIKit
import AuthenticationServices
import Firebase
import FirebaseAuth
import CryptoKit


class AppleSignInCoordinator: NSObject, Coordinator {
    
    fileprivate var navigationController: UINavigationController
    internal var coordinators: [Coordinator] = []
    
    weak var delegate: ServiceSignInCoordinatorDelegate?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startSignInWithAppleFlow()
    }
    
    
    // code below grabbed from https://firebase.google.com/docs/auth/ios/apple
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if length == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?
    
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
}

extension AppleSignInCoordinator: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                print("Invalid state: A login callback was received, but no login request was sent.")
                return
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            func signIn(credential: AuthCredential) {
                Auth.auth().signIn(with: credential) { [unowned self] (authResult, error) in
                    if let error = error {
                        self.navigationController.visibleViewController?.present(error: error)
                        self.delegate?.coordinatorDidCancel(self)
                    } else {
                        self.delegate?.coordinatorDidSignIn(self)
                    }
                }
            }
            
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            
            let currentUser = AppState.shared.currentUser
            if currentUser?.isAnonymous ?? false {
                (currentUser as? User)?.link(with: credential, completion: { (authResult, error) in
                    let err = error as NSError?
                    if err == nil {
                        self.delegate?.coordinatorDidSignIn(self)
                    } else if err?.domain == AuthErrorDomain, err?.code == AuthErrorCode.credentialAlreadyInUse.rawValue, let udpatedCredential = err?.userInfo[AuthErrorUserInfoUpdatedCredentialKey] as? OAuthCredential {
                        // need to get updatedCredential, https://github.com/firebase/firebase-ios-sdk/issues/4434
                        signIn(credential: udpatedCredential)
                    } else {
                        self.navigationController.visibleViewController?.present(error: error!)
                        self.delegate?.coordinatorDidCancel(self)
                    }
                })
            } else {
                signIn(credential: credential)
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
        self.navigationController.visibleViewController?.present(error: error)
        delegate?.coordinatorDidCancel(self)
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return navigationController.visibleViewController?.view.window ?? (UIApplication.shared.delegate as! AppDelegate).window!
    }
}
