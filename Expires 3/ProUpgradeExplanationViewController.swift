//
//  ProUpgradeExplanationViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

class ProUpgradeExplanationViewController: UIViewController {

    var viewModel: UpgradeExplanationViewModelType!
    
    @IBOutlet weak var upgradeButton: ColoredButton!
    
    @IBOutlet var allLabels: [UILabel]!
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .expLightLightViolet
        
        allLabels.forEach {
            $0.textColor = .expViolet2
        }
        upgradeButton.applyRoundedStyle(for: UIColor.expViolet.withAlphaComponent(0.9), borderColor: .expViolet2)
        upgradeButton.setTitleColor(.expLightLightViolet, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.isUpgradeAvailable.bindAndFire { (isAvailable) in
            self.upgradeButton.isEnabled = isAvailable
        }
        viewModel.upgradeButtonTitle.bindAndFire { (buttonTitle) in
            self.upgradeButton.setTitle(buttonTitle, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .proUpgradeScreenShown)
    }
    
    @IBAction func upgradeButtonTouched(_ sender: Any) {
        appAnalytics.log(event: .proUpgradeScreenUpgradeButtonTouched)
        viewModel.purchaseUpgrade()
    }
}

extension ProUpgradeExplanationViewController {
    class func create(from storyboard: UIStoryboard, with viewModel: UpgradeExplanationViewModelType) -> ProUpgradeExplanationViewController {
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProUpgradeExplanationViewController") as! ProUpgradeExplanationViewController
        viewController.viewModel = viewModel
        return viewController
    }
}
