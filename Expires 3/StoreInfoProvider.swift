//
//  StoreInfoProvider.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

protocol StoreInfoProviderType: class { // want to be class because dont want to copy instance when passed to method parametr
    var priceInfo: (price: Double, locale: Locale)? { get set }
    var priceChanged: (() -> Void)? { get set }
}

class StoreInfoProvider: StoreInfoProviderType {
    var priceInfo: (price: Double, locale: Locale)? {
        didSet {
            priceChanged?()
        }
    }
    
    var priceChanged: (() -> Void)?
    
}
