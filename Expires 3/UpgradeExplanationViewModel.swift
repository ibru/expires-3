//
//  UpgradeExplanationViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

protocol UpgradeExplanationViewModelDelegate: class {
    func viewModelWantsToPurchaseUpgrade(_ viewModel: UpgradeExplanationViewModelType)
}
protocol UpgradeExplanationViewModelType {
    var delegate: UpgradeExplanationViewModelDelegate? { get set }
    
    var priceString: Dynamic<String?> { get }
    var upgradeButtonTitle: Dynamic<String> { get }
    var isUpgradeAvailable: Dynamic<Bool> { get }
    
    func purchaseUpgrade()
}

class UpgradeExplanationViewModel: UpgradeExplanationViewModelType {

    fileprivate var infoProvider: StoreInfoProviderType
    
    weak var delegate: UpgradeExplanationViewModelDelegate?
    
    var priceString: Dynamic<String?> = Dynamic(nil)
    
    var upgradeButtonTitle: Dynamic<String> = Dynamic(NSLocalizedString("Upgrade", comment: ""))
    
    var isUpgradeAvailable: Dynamic<Bool> = Dynamic(false)
    
    init(storeInfoProvider: StoreInfoProviderType) {
        infoProvider = storeInfoProvider
        priceString.value = localizedPrice(from: infoProvider)
        isUpgradeAvailable.value = isUpgradeAvailable(from: infoProvider)
        upgradeButtonTitle.value = upgradeButtonTitle(from: infoProvider)
        
        infoProvider.priceChanged = {
            DispatchQueue.main.async {
                self.priceString.value = self.localizedPrice(from: self.infoProvider)
                self.isUpgradeAvailable.value = self.isUpgradeAvailable(from: self.infoProvider)
                self.upgradeButtonTitle.value = self.upgradeButtonTitle(from: self.infoProvider)
            }
        }
    }
    
    func purchaseUpgrade() {
        guard isUpgradeAvailable.value else { return }
        
        delegate?.viewModelWantsToPurchaseUpgrade(self)
    }
}

extension UpgradeExplanationViewModel {
    fileprivate func localizedPrice(from provider: StoreInfoProviderType) -> String? {
        if let priceInfo = provider.priceInfo {
            return localizedPrice(from: priceInfo.price, locale: priceInfo.locale)
        } else {
            return nil
        }
    }
    
    fileprivate func localizedPrice(from price: Double, locale: Locale) -> String {
        if price == 0 {
            return "Free"
        }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale
        return numberFormatter.string(from: NSNumber(value: price)) ?? "N/A"
    }
    
    fileprivate func isUpgradeAvailable(from provider: StoreInfoProviderType) -> Bool {
        return provider.priceInfo != nil
    }
    
    fileprivate func upgradeButtonTitle(from provider: StoreInfoProviderType) -> String {
        if let priceInfo = provider.priceInfo {
            return NSLocalizedString("Upgrade for ", comment: "") + localizedPrice(from: priceInfo.price, locale: priceInfo.locale)
        } else {
            return NSLocalizedString("Upgrade", comment: "")
        }
    }
}
