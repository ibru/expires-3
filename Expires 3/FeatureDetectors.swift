//
//  FeatureDetectors.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 02/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

protocol LocalIAPValidator {
    func localyValidatedReceiptProducts() -> Set<IAPProduct>
}
protocol RemoteIAPValidator {
    func validateReceipt(completion: ((_ products: Set<IAPProduct>) -> Void)?)
}
protocol UserDataFeatureChecker {
    func checkTrasferedFeatures(completion: ((_ features: Set<AppFeature>, _ error: Error?) -> Void)?)
}

protocol IAPProductToFeatureTranslatable {
    func features(from products: Set<IAPProduct>) -> Set<AppFeature>
}
extension IAPProductToFeatureTranslatable {
    func features(from products: Set<IAPProduct>) -> Set<AppFeature> {
        var features: Set<AppFeature> = []
        
        for product in products {
            if product == .expiresPro {
                features.insert(.unlimitedItemsCount)
                features.insert(.extendedExpirationItemInfo)
            }
        }
        return features
    }
}

class LocalAppFeaturesDetector: AppFeaturesLocalDetectable, IAPProductToFeatureTranslatable {
    
    fileprivate let iapValidator: LocalIAPValidator
    
    init(validator: LocalIAPValidator) {
        self.iapValidator = validator
    }
    
    func localyEnabledFeatures() -> Set<AppFeature> {
        let iapProducts = iapValidator.localyValidatedReceiptProducts()
        let keychainStoredFeatures = [""] // TODO: implement
        
        var enabledFeatures: Set<AppFeature> = []
        
        features(from: iapProducts).forEach { enabledFeatures.insert($0) }
        
        // TODO: check keychain for other features
        
        return enabledFeatures
    }
}

class RemoteAppFeaturesDetector: AppFeaturesRemoteDetectable, IAPProductToFeatureTranslatable {
    
    init(iapValidator: RemoteIAPValidator, userDataChecker: UserDataFeatureChecker) {
        self.iapValidator = iapValidator
        self.userDataChecker = userDataChecker
    }
    
    let iapValidator: RemoteIAPValidator
    let userDataChecker: UserDataFeatureChecker
    
    func remotelyEnabledFeatures(completion: ((Set<AppFeature>, Error?) -> Void)?) {
        var enabledFeatures: Set<AppFeature> = []
        let group = DispatchGroup()
        var validationError: Error?
        
        group.enter()
        iapValidator.validateReceipt { (products) in
            self.features(from: products).forEach { enabledFeatures.insert($0) }
            group.leave()
        }
        
        group.enter()
        userDataChecker.checkTrasferedFeatures { features, error in
            features.forEach { enabledFeatures.insert($0) }
            validationError = error
            group.leave()
        }
        group.notify(queue: DispatchQueue.main) {
            completion?(enabledFeatures, validationError)
        }
    }
}

class UserAuthModelFeaturesChecker: UserDataFeatureChecker {
    fileprivate let model: UserAuthModel
    
    init(model: UserAuthModel) {
        self.model = model
    }
    
    func checkTrasferedFeatures(completion: ((Set<AppFeature>, Error?) -> Void)?) {
        guard let user = model.currentUser else {
            completion?([], nil)
            return
        }
        model.additionalInfo(forUser: user) { (userInfo) in
            var features = Set<AppFeature>()
            
            if let migrationInfo = userInfo?.expires3Migration {
                features = self.features(from: migrationInfo)
            }
            completion?(features, nil)
        }
    }
    
    func features(from migrationInfo: Expires3MigrationInfo) -> Set<AppFeature> {
        // make Expires 2 paid users pay again, sorry guys
//        if !migrationInfo.wasLiteUser || migrationInfo.wasPaidUser {
//            return [.unlimitedItemsCount]
//        }
        return []
    }
}

// seems this code doesnt work
/*func +(left: Set<AppFeature>, right: Set<AppFeature>) -> Set<AppFeature> {
    var total = Set<AppFeature>()
    left.forEach {
        total.insert($0)
    }
    right.forEach {
        total.insert($0)
    }
    return total
}
func +=(left: inout Set<AppFeature>, right: Set<AppFeature>) {
    left = left + right
}*/
