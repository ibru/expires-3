//
//  Dynamic.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 18/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

class Dynamic<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    func bind(to listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(to listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    func fire() {
        listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v: T) {
        value = v
    }
}
