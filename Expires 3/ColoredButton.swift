//
//  ColoredButton.swift
//  TRDR
//
//  Created by Jiri Urbasek on 10/02/15.
//  Copyright (c) 2015 TRDR. All rights reserved.
//

import UIKit

class ColoredButton: UIButton {
    private var bgColors = [UInt: UIColor?]()
    private var borderColors = [UInt: UIColor?]()
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            if newValue {
                if let color = bgColors[UIControl.State.highlighted.rawValue] {
                    backgroundColor = color
                }
                if let color = borderColors[UIControl.State.highlighted.rawValue] {
                    layer.borderColor = color?.cgColor
                }
            }
            else {
                setNormalBGColor()
                setNormalBorderColor()
            }
            super.isHighlighted = newValue
        }
    }
    
    override var isEnabled: Bool {
        get {
            return super.isEnabled
        }
        set {
            if newValue {
                setNormalBGColor()
                setNormalBorderColor()
            }
            else {
                if let color = bgColors[UIControl.State.disabled.rawValue] {
                    backgroundColor = color
                }
                if let color = borderColors[UIControl.State.disabled.rawValue] {
                    layer.borderColor = color?.cgColor
                }
            }
            super.isEnabled = newValue
        }
    }
    
    
    func setBackgroundColor(color: UIColor?, forState state: UIControl.State) {
        bgColors[state.rawValue] = color
    }
    func setBorderColor(color: UIColor?, forState state: UIControl.State) {
        borderColors[state.rawValue] = color
    }

    
    private func setNormalBGColor() {
        if let color = bgColors[UIControl.State.normal.rawValue] {
            backgroundColor = color
        }
    }
    
    private func setNormalBorderColor() {
        if let color = borderColors[UIControl.State.normal.rawValue] {
            layer.borderColor = color?.cgColor
        }
    }
}

extension ColoredButton {
    func applyRoundedStyle(for bgColor: UIColor, borderColor: UIColor) {
        layer.borderWidth = 0.5
        layer.cornerRadius = 8
        setBackgroundColor(color: bgColor, forState: .normal)
        setBackgroundColor(color: bgColor.withAlphaComponent(0.7), forState: .highlighted)
        setBackgroundColor(color: bgColor.withAlphaComponent(0.5), forState: .disabled)
        setBorderColor(color: borderColor, forState: .normal)
        setBorderColor(color: borderColor.withAlphaComponent(0.4), forState: .highlighted)
        setBorderColor(color: borderColor.withAlphaComponent(0.4), forState: .disabled)
        isEnabled = true
    }
}
