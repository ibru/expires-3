//
//  FacebookSignInViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit


protocol FacebookSignInViewModel {
    var userToLink: DatabaseUser? { get set }
    var signedInUser: Dynamic<DatabaseUser?> { get set }
    var signInError: Dynamic<Error?> { get set }
    
    func signIn(from viewController: UIViewController)
}

struct FacebookSignInViewModelImpl: FacebookSignInViewModel {
    var userToLink: DatabaseUser?
    var signedInUser: Dynamic<DatabaseUser?> = Dynamic(nil)
    var signInError: Dynamic<Error?> = Dynamic(nil)
    
    func signIn(from viewController: UIViewController) {        
        if let token = AccessToken.current {
            signIn(with: token)
        } else {
            LoginManager().logIn(permissions: ["public_profile"], from: viewController) { (result, error) in
                guard let result = result else {
                    self.didFinishSignIn(user: nil, error: error)
                    return
                }
                if result.isCancelled {
                    self.didFinishSignIn(user: nil, error: nil)
                } else if let token = result.token {
                    self.signIn(with: token)
                }
            }
        }
    }
    
    fileprivate func signIn(with token: AccessToken) {
        let credential = token.authCredential
        
        if let linkUser = userToLink as? User {
            linkUser.link(with: credential) { (result, error) in
                let err = error as NSError?
                if err?.domain == AuthErrorDomain, err?.code == AuthErrorCode.credentialAlreadyInUse.rawValue {
                    self.signIn(with: credential)
                } else {
                    self.didFinishSignIn(user: result?.user, error: error)
                }
            }
        } else {
            signIn(with: credential)
        }
    }
    
    fileprivate func signIn(with credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (result, error) in
            self.didFinishSignIn(user: result?.user, error: error)
        }
    }
    
    fileprivate func didFinishSignIn(user: DatabaseUser?, error: Error?) {
        signedInUser.value = user
        signInError.value = error
    }
}

extension AccessToken {
    var authCredential: AuthCredential {
        return FacebookAuthProvider.credential(withAccessToken: self.tokenString)
    }
}

