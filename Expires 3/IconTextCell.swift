//
//  IconTextCell.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 27/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class IconTextCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.layoutMargins = UIEdgeInsets(top: 8, left: 30, bottom: 8, right: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
