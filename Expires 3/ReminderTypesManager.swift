//
//  ReminderTypesManager.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 20/03/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseFirestore


public protocol ReminderTypesManagerDelegate {
    func manager(_ manager: ReminderTypesManager, didLoadPublicTypes types: [String], forLanguage languageCode: String)
    func manager(_ manager: ReminderTypesManager, didUpdatePrivateTypes types: [String]?)
    func manager(_ manager: ReminderTypesManager, didReceiveError error: Error)
}

public protocol ReminderTypesManager {
    var delegate: ReminderTypesManagerDelegate? { get set }
    
    init(userId: String?)
    
    var publicTypes: [String]? { get }
    var hasPrivateTypes: Bool  { get }
    
    func loadPublicTypes(forLanguage deviceLangCode: String)
    func observePrivateTypes()
    
    func migratePublicTypesIntoPrivateTypes(addingType additionalType: String?)
    func add(privateType type: String)
    func delete(privateType type: String)
    func move(privateType type: String, toOrder order: Int)
    func has(privateType type: String) -> Bool
}

public class FirebaseReminderTypesManager: ReminderTypesManager {
    
    public var delegate: ReminderTypesManagerDelegate?
    
    public var hasPrivateTypes: Bool {
        return privateTypes != nil
    }
    
    private var publicTypesRef: DatabaseReference = Database.database().reference().child(.publicTypes)
    private var privateTypesRef: DatabaseReference?
    private var privateRefHandle: UInt?
    
    public var publicTypes: [String]?
    private var privateTypes: [String]?
    
    public required init(userId: String?) {
        if let userId = userId {
            privateTypesRef = Database.database().reference().child(.privateTypes(userId: userId))
        }
    }
    
    public func loadPublicTypes(forLanguage deviceLangCode: String) {
        let defaultLangCode = "en"
        
        publicTypesRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let code = snapshot.hasChild(deviceLangCode) ? deviceLangCode : defaultLangCode
            
            if let types = snapshot.childSnapshot(forPath: code).value as? [String] {
                self.publicTypes = types
                self.delegate?.manager(self, didLoadPublicTypes: types, forLanguage: code)
            }
        }) { error in
            self.delegate?.manager(self, didReceiveError: error)
        }
    }
    
    public func observePrivateTypes() {
        
        guard privateRefHandle == nil else {
            delegate?.manager(self, didUpdatePrivateTypes: privateTypes)
            return
        }
        
        privateRefHandle = privateTypesRef?.observe(.value, with: { (snapshot) in

            if let types = snapshot.value as? [JSONDict] {
                var privateTypes = [String]()
                
                for typeDict in types {
                    if let type = ReminderType(id: "_unsupported", dictionary: typeDict) {
                        privateTypes.append(type.title)
                    }
                }
                self.privateTypes = privateTypes
            }
            self.delegate?.manager(self, didUpdatePrivateTypes: self.privateTypes)
            
        }) { error in
            self.delegate?.manager(self, didReceiveError: error)
        }
    }
    
    public func updatePrivateTypes(privateTypes: [String]) {
        var reminderTypes = [JSONDict]()
        for (order, type) in privateTypes.enumerated() {
            reminderTypes.append(ReminderType(title: type, order: order).dictValue)
        }
        
        privateTypesRef?.setValue(reminderTypes) { (error, reference) in
            if let error = error {
                self.delegate?.manager(self, didReceiveError: error)
            }
        }
    }
    
    public func migratePublicTypesIntoPrivateTypes(addingType additionalType: String?) {
        guard var newPrivateTypes = publicTypes else {
            return //TODO: we should try to fetch them again because public types always exist
        }
        if let newType = additionalType {
            newPrivateTypes.insert(newType, at: 0)
        }
        updatePrivateTypes(privateTypes: newPrivateTypes)
    }
    
    public func add(privateType type: String) {
        guard var privateTypes = privateTypes else {
            return
        }
        
        privateTypes.insert(type, at: 0)
        updatePrivateTypes(privateTypes: privateTypes)
    }
    
    public func delete(privateType type: String) {
        guard var privateTypes = privateTypes else {
            return
        }
        
        if let idx = privateTypes.firstIndex(of: type) {
            privateTypes.remove(at: idx)
            updatePrivateTypes(privateTypes: privateTypes)
        }
    }
    
    /**
     @param order starting from 0
     */
    public func move(privateType type: String, toOrder order: Int) {
        guard var privateTypes = privateTypes else {
            return
        }
        
        if let idx = privateTypes.firstIndex(of: type) {
            privateTypes.insert(privateTypes.remove(at: idx), at: order)
            updatePrivateTypes(privateTypes: privateTypes)
        }
    }
    
    public func has(privateType type: String) -> Bool {
        return privateTypes?.contains(type) ?? false
    }
}

enum FirestoreTypesManagerError: Error {
    case couldNotLoadPublicExpirationTypes
    case couldNotLoadPrivateExpirationTypes
}

public class FirestoreReminderTypesManager: ReminderTypesManager {
    
    public var delegate: ReminderTypesManagerDelegate?
    
    public var hasPrivateTypes: Bool {
        return privateTypes != nil
    }
    
    private var publicTypesRef: CollectionReference = Firestore.firestore().collection("expirationTypes")
    private var privateTypesRef: CollectionReference?
    private var privateTypesListener: ListenerRegistration?
    
    public var publicTypes: [String]?
    private var privateTypes: [ReminderType]?
    
    public required init(userId: String?) {
        if let userId = userId {
            privateTypesRef = Firestore.firestore().collection("users").document(userId).collection("expirationTypes")
        }
    }
    
    public func loadPublicTypes(forLanguage deviceLangCode: String) {
        let defaultLangCode = "en"
        
        publicTypesRef.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                DispatchQueue.main.async { self.delegate?.manager(self, didReceiveError: error ?? FirestoreTypesManagerError.couldNotLoadPublicExpirationTypes) }
                return
            }
            
            let documents = snapshot.documents.filter { $0.documentID == deviceLangCode || $0.documentID == defaultLangCode }
            
            guard documents.count > 0 else {
                DispatchQueue.main.async { self.delegate?.manager(self, didReceiveError: FirestoreTypesManagerError.couldNotLoadPublicExpirationTypes) }
                return
            }
            
            let document: DocumentSnapshot
            if let idx = documents.firstIndex(where: { $0.documentID == deviceLangCode }) {
                document = documents[idx]
            } else {
                document = documents.first!
            }
                
            guard let types = document.data()?["types"] as? [String] else {
                DispatchQueue.main.async { self.delegate?.manager(self, didReceiveError: FirestoreTypesManagerError.couldNotLoadPublicExpirationTypes) }
                return
            }
            self.publicTypes = types
            DispatchQueue.main.async { self.delegate?.manager(self, didLoadPublicTypes: types, forLanguage: document.documentID) }
        }
    }
    
    public func observePrivateTypes() {
        guard privateTypesListener == nil else {
            delegate?.manager(self, didUpdatePrivateTypes: privateTypes?.map { $0.title })
            return
        }
        privateTypesListener = privateTypesRef?.order(by: "order").addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot else {
                DispatchQueue.main.async { self.delegate?.manager(self, didReceiveError: FirestoreTypesManagerError.couldNotLoadPrivateExpirationTypes) }
                return
            }
            var items = [ReminderType]()
            snapshot.documents.forEach {
                guard let item = ReminderType(id: $0.documentID, dictionary: $0.data()) else { return }
                items.append(item)
            }
            self.privateTypes = items.isEmpty ? nil : items // zero private types means user has no private types at all
            
            let types = self.privateTypes?.map { $0.title }
            DispatchQueue.main.async { self.delegate?.manager(self, didUpdatePrivateTypes: types) }
        }
    }
    
    func updatePrivateTypes(newTypes: [String]) {
        guard let collectionRef = privateTypesRef else { return }
        
        let batch = collectionRef.firestore.batch()
        
        privateTypes?.forEach {
            batch.deleteDocument(collectionRef.document($0.id))
        }
        var newPrivateTypes = [ReminderType]()
        for (order, type) in newTypes.enumerated() {
            let ref = collectionRef.document()
            let id = ref.documentID
            let reminderType = ReminderType(id: id, title: type, order: order)
            
            batch.setData(reminderType.dictValue, forDocument: ref)
            newPrivateTypes.append(reminderType)
        }
        batch.commit { (error) in
            if let error = error {
                self.delegate?.manager(self, didReceiveError: error)
            }
        }
    }
    
    public func migratePublicTypesIntoPrivateTypes(addingType additionalType: String?) {
        guard var newTypes = publicTypes else { return }
        
        if let newType = additionalType {
            newTypes.insert(newType, at: 0)
        }
        updatePrivateTypes(newTypes: newTypes)
    }
    
    public func add(privateType type: String) {
        guard let privateTypes = privateTypes else { return }
        
        var newTypes = privateTypes.map { $0.title }
        newTypes.insert(type, at: 0)
        
        updatePrivateTypes(newTypes: newTypes)
    }
    
    public func delete(privateType type: String) {
        guard var privateTypes = (privateTypes?.map { $0.title }) else {
            return
        }
        
        if let idx = privateTypes.firstIndex(of: type) {
            privateTypes.remove(at: idx)
            updatePrivateTypes(newTypes: privateTypes)
        }
    }
    
    /**
     @param order starting from 0
     */
    public func move(privateType type: String, toOrder order: Int) {
        guard var privateTypes = (privateTypes?.map { $0.title }) else {
            return
        }
        
        if let idx = privateTypes.firstIndex(of: type) {
            privateTypes.insert(privateTypes.remove(at: idx), at: order)
            updatePrivateTypes(newTypes: privateTypes)
        }
    }
    
    public func has(privateType type: String) -> Bool {
        return privateTypes?.contains { $0.title == type } ?? false
    }
}
