//
//  AlertHappySmileViewController.swift
//  Tradewise
//
//  Created by Jiri Urbasek on 06/01/2017.
//  Copyright © 2017 TRDR. All rights reserved.
//

import UIKit

class AlertNotificationsAuthorizationViewController: UIViewController, AnimatableController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionButton: ColoredButton!
    @IBOutlet weak var notNowButton: ColoredButton!
    
    typealias ButtonActionHandler = (_ completion: @escaping ((_ canDismiss: Bool) -> Void)) -> Void
    
    var alertTitle: String? {
        didSet {
            titleLabel?.text = alertTitle
        }
    }
    var alertSubtitle: String? {
        didSet {
            subtitleLabel?.text = alertSubtitle
        }
    }
    var actionButtonTitle: String? {
        didSet {
            actionButton?.setTitle(actionButtonTitle, for: .normal)
        }
    }
    
    var doActionHandler: ButtonActionHandler?
    var notNowHandler: ButtonActionHandler?
    
    override func loadView() {
        super.loadView()
        
        containerView.backgroundColor = .expLightLightViolet
        
        [titleLabel, subtitleLabel].forEach {
            $0?.textColor = .expViolet2
        }
        actionButton.applyRoundedStyle(for: UIColor.expViolet.withAlphaComponent(0.9), borderColor: .expViolet2)
        actionButton.setTitleColor(.expLightLightViolet, for: .normal)
        
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.expViolet2,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        notNowButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("Not now", comment: ""), attributes: attrs), for: .normal)
        notNowButton.setTitleColor(.expViolet2, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = alertTitle
        subtitleLabel.text = alertSubtitle
        actionButton?.setTitle(actionButtonTitle, for: .normal)
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.60)
        
        containerView.layer.cornerRadius = 12
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func notNowButtonTouched(_ sender: Any) {
        notNowHandler?() { canDismiss in
            if canDismiss {
                self.dismissAlert()
            }
        }
    }
    
    @IBAction func actionButtonTouched(_ sender: Any) {
        doActionHandler?() { canDismiss in
            if canDismiss {
                self.dismissAlert()
            }
        }
        
    }
}
