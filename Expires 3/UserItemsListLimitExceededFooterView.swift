//
//  UserItemsListLimitExceededFooterView.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 04/05/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

class UserItemsListLimitExceededFooterView: UICollectionReusableView {
        
    @IBOutlet weak var upgradeButton: ColoredButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = NSLocalizedString("Maximum items count reached. Upgrade to Full version to have unlimited number of items and more features in the future.", comment: "")
        titleLabel.textColor = .expWhite
        upgradeButton.applyRoundedStyle(for: .clear, borderColor: .expWhite)
        upgradeButton.setTitleColor(.expWhite, for: .normal)
        upgradeButton.setBackgroundColor(color: .clear, forState: .highlighted)
        upgradeButton.setBackgroundColor(color: .clear, forState: .disabled)
    }
}
