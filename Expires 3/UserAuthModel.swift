//
//  UserAuthModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 20/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

struct UserAdditionalInfo {
    let expires3Migration: Expires3MigrationInfo?
    
    init?(data: [String: Any]) {
        if let migrationData = data["expires3Migration"] as? [String: Any], let migrationInfo = Expires3MigrationInfo(data: migrationData) {
            expires3Migration = migrationInfo
        } else {
            expires3Migration = nil
        }
    }
}

struct Expires3MigrationInfo {
    enum UserAdditionalInfoKey: String {
        case date = "date"
        case wasPaidUser = "wasPaidUser"
        case wasLiteUser = "wasLiteVersion"
        case itemsCount = "itemsCount"
        
    }
    
    let date: Date
    let wasPaidUser: Bool
    let wasLiteUser: Bool
    let itemsCount: Int
    
    init?(data: [String: Any]) {
        guard let date = data[UserAdditionalInfoKey.date.rawValue] as? Date, let itemsCount = data[UserAdditionalInfoKey.itemsCount.rawValue] as? Int else {
            return nil
        }
        self.date = date
        self.itemsCount = itemsCount
        self.wasLiteUser = data[UserAdditionalInfoKey.wasLiteUser.rawValue] as? Bool ?? true
        self.wasPaidUser = data[UserAdditionalInfoKey.wasPaidUser.rawValue] as? Bool ?? false
    }
}

protocol UserAuthModel: class {
    var currentUser: DatabaseUser? { get }
    
    var userAuthChanged: (() -> Void)? { get set }
    var userAdditionalInfoChanged: ((_ info: UserAdditionalInfo) -> Void)? { get set }
    
    func additionalInfo(forUser user: DatabaseUser, completion: ((_ info: UserAdditionalInfo?) -> Void)?)
    
    func signInAnonymously(completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?)
    func signIn(withCredential credential: AuthCredential, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?)
    func link(credential: AuthCredential, toUser user: User, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?)
    func signOut(completion: (_ success: Bool, _ error: Error?) -> Void)
}

class FirebaseUserAuthModel: UserAuthModel {
    
    var userAuthChanged: (() -> Void)?
    
    var userAdditionalInfoChanged: ((_ info: UserAdditionalInfo) -> Void)?
    
    init() {
        Auth.auth().addStateDidChangeListener { [weak self] auth, user in
            self?.userAuthChanged?()
        }
    }
    
    func additionalInfo(forUser user: DatabaseUser, completion:  ((UserAdditionalInfo?) -> Void)?) {
        completion?(nil)
    }
    
    var currentUser: DatabaseUser? { return Auth.auth().currentUser }
    
    func signInAnonymously(completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        Auth.auth().signInAnonymously() { (result, error) in
            if result?.user.uid == nil {
                completion?(nil, error)
            } else {
                completion?(result?.user, error)
            }
        }
    }
    
    func signIn(withCredential credential: AuthCredential, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        Auth.auth().signIn(with: credential) { (result, error) in
            completion?(result?.user, error)
        }
    }
    
    func link(credential: AuthCredential, toUser user: User, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        user.link(with: credential)  { (result, error) in
            completion?(result?.user, error)
        }
    }
    
    func signOut(completion: (_ success: Bool, _ error: Error?) -> Void) {
        guard let providers = Auth.auth().currentUser?.providerData else { return }
        
        providers.forEach {
            Auth.auth().currentUser?.unlink(fromProvider: $0.providerID, completion: { (user, error) in
                
            })
        }
    }
}

class FirestoreUserAuthModel: UserAuthModel {
    
    private var listener: ListenerRegistration?
    
    private var userAdditionalInfo: UserAdditionalInfo?
    
    var userAuthChanged: (() -> Void)?
    
    var userAdditionalInfoChanged: ((_ info: UserAdditionalInfo) -> Void)?
    
    init() {
        Auth.auth().addStateDidChangeListener { [weak self] auth, user in
            DispatchQueue.main.async { self?.userAuthChanged?() }
            
            if let userId = user?.id {
                self?.listener?.remove()
                self?.listener = Firestore.firestore().collection("users").document(userId).addSnapshotListener({ (snapshot, error) in
                    if let snapshotData = snapshot?.data(), let info = UserAdditionalInfo(data: snapshotData) {
                        self?.userAdditionalInfo = info
                        DispatchQueue.main.async { self?.userAdditionalInfoChanged?(info) }
                    }
                })
            } else {
                self?.listener?.remove()
                self?.listener = nil
            }
        }
    }
    
    func additionalInfo(forUser user: DatabaseUser, completion: ((UserAdditionalInfo?) -> Void)?) {
        completion?(self.userAdditionalInfo)
    }
    
    var currentUser: DatabaseUser? { return Auth.auth().currentUser }
    
    func signInAnonymously(completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        Auth.auth().signInAnonymously() { (result, error) in
            if result?.user.uid == nil {
                completion?(nil, error)
            } else {
                completion?(result?.user, error)
            }
        }
    }
    
    func signIn(withCredential credential: AuthCredential, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        Auth.auth().signIn(with: credential) { (result, error) in
            completion?(result?.user, error)
        }
    }
    
    func link(credential: AuthCredential, toUser user: User, completion: ((_ user: DatabaseUser?, _ error: Error?) -> Void)?) {
        user.link(with: credential)  { (result, error) in
            completion?(result?.user, error)
        }
    }
    
    func signOut(completion: (_ success: Bool, _ error: Error?) -> Void) {
        guard let providers = Auth.auth().currentUser?.providerData else { return }
        
        providers.forEach {
            Auth.auth().currentUser?.unlink(fromProvider: $0.providerID, completion: { (user, error) in
                
            })
        }
    }
}
