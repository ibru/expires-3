//
//  CreateExpirationItemViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 20/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

protocol CreateExpirationItemViewModelDelegate: class {
    func viewModel(_ viewModel: CreateExpirationItemViewModel, didCreateItem item: ExpirationItem)
}

protocol CreateExpirationItemViewModelType {
    var delegate: CreateExpirationItemViewModelDelegate? { get set }
    
    var reminderTypesManager: ReminderTypesManager? { get set }
    
    var itemType: String? { get set }
    var expirationDate: Date? { get set }
    var reminderIntervals: [Int]? { get set }
    var notes: String? { get set }
    
    var canCreateItem: Dynamic<Bool> { get }
    var itemCreationError: Dynamic<Error?> { get }
    func createItem()
}

class CreateExpirationItemViewModel: CreateExpirationItemViewModelType {
    
    fileprivate let authModel: UserAuthModel = AppState.shared.modelFactory.userAuthModel()
    
    weak var delegate: CreateExpirationItemViewModelDelegate?
    
    var reminderTypesManager: ReminderTypesManager?
    
    var itemType: String? {
        didSet {
            checkIfCanCreateItem()
        }
    }
    
    var expirationDate: Date? {
        didSet {
            checkIfCanCreateItem()
        }
    }
    
    var reminderIntervals: [Int]? {
        didSet {
            checkIfCanCreateItem()
        }
    }
    
    var notes: String? {
        didSet {
            checkIfCanCreateItem()
        }
    }
    
    var canCreateItem = Dynamic(false)
    var itemCreationError = Dynamic<Error?>(nil)
    
    func createItem() {
        guard canCreateItem.value, let title = itemType, let date = expirationDate else { return }
        
        let createdExpirationItem = ExpirationItem(id: ExpirationItem.unsavedItemId, title: title, date: date, reminders: reminderIntervals, notes: notes)
        
        guard let user = authModel.currentUser else {
            authModel.signInAnonymously(completion: { (user, error) in
                if let user = user {
                    self.save(newItem: createdExpirationItem, forUser: user)
                } else {
                    self.itemCreationError.value = error
                }
            })
            return
        }
        save(newItem: createdExpirationItem, forUser: user)
    }
    
    private func checkIfCanCreateItem() {
        canCreateItem.value = itemType != nil && itemType?.isEmpty == false && expirationDate != nil
    }
    
    private func save(newItem item: ExpirationItem, forUser user: DatabaseUser) {
        
        if let reminderTypesManager = reminderTypesManager {
            if reminderTypesManager.hasPrivateTypes {
                if let type = itemType, !reminderTypesManager.has(privateType: type) {
                    reminderTypesManager.add(privateType: type)
                }
            } else {
                var addedType: String? = nil
                if let type = itemType, let publicTypes = reminderTypesManager.publicTypes {
                    if !publicTypes.contains(type) {
                        addedType = type
                    }
                }
                reminderTypesManager.migratePublicTypesIntoPrivateTypes(addingType: addedType)
            }
        }
        
        let itemModel: ExpirationItemModel = AppState.shared.modelFactory.expirationItemModel(userId: user.id)
        itemModel.create(newItem: item) { (item, error) in
            if let item = item {
                self.delegate?.viewModel(self, didCreateItem: item)
            } else {
                self.itemCreationError.value = error
            }
        }
    }
}
