//
//  ModelsFactory.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 24/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

enum ModelsFactory {
    case firebase, firestore
    
    func userAuthModel() -> UserAuthModel {
        switch self {
        case .firebase: return FirebaseUserAuthModel()
        case .firestore: return FirestoreUserAuthModel()
        }
    }
    func userExpirationItemsModel(userId: String) -> UserExpirationItemsModelType {
        switch self {
        case .firebase: return FirebaseExpirationItemsModel(userId: userId)
        case .firestore: return FirestoreExpirationItemsModel(userId: userId)
        }
    }
    func expirationItemModel(userId: String) -> ExpirationItemModel {
        switch self {
        case .firebase: return FirebaseExpirationItemModel(userId: userId)
        case .firestore: return FirestoreExpirationItemModel(userId: userId)
        }
    }
    func reminderTypesManager(userId: String?) -> ReminderTypesManager {
        switch self {
        case .firebase: return FirebaseReminderTypesManager(userId: userId)
        case .firestore: return FirestoreReminderTypesManager(userId: userId)
        }
    }
}
