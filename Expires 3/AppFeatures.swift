//
//  AppFeatures.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 02/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation


enum AppFeature {
    case unlimitedItemsCount // enabled by purchasing IAP and for paid users migrated from Expires 2
    case extendedExpirationItemInfo // enabled by purchasing IAP only
}

protocol AppFeaturesLocalDetectable {
    func localyEnabledFeatures() -> Set<AppFeature>
}
protocol AppFeaturesRemoteDetectable {
    func remotelyEnabledFeatures(completion: ((_ features: Set<AppFeature>, _ error: Error?) -> Void)?)
}

class AppFeaturesManager {
    
    enum TypeOfFeatureValidation {
        case local, remote
    }
    
    fileprivate let localFeaturesDetector: AppFeaturesLocalDetectable
    fileprivate let remoteFeaturesDetector: AppFeaturesRemoteDetectable
    
    init(localDetector: AppFeaturesLocalDetectable, remoteDetector: AppFeaturesRemoteDetectable) {
        localFeaturesDetector = localDetector
        remoteFeaturesDetector = remoteDetector
    }
    
    /**
     callback might be called multiple times - for local detection and then when verified by remote detection
     */
    func detectEnabledFeatures(completion: ((_ features: Set<AppFeature>, _ type: TypeOfFeatureValidation) -> Void)?) {
        let features = localFeaturesDetector.localyEnabledFeatures()
        completion?(features, .local)
        
        remoteFeaturesDetector.remotelyEnabledFeatures { features, error in
            completion?(features, .remote)
        }
    }
}
