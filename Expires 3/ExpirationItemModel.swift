//
//  ExpirationItemModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 20/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseFirestore

protocol ExpirationItemModel {
    init(userId: String)
    func create(newItem item: ExpirationItem, completion: ((_ expirationItem: ExpirationItem?, _ error: Error?) -> Void)?)
    func updateValuesFor(item: ExpirationItem, completion: ((_ expirationItem: ExpirationItem?, _ error: Error?) -> Void)?)
    func delete(item: ExpirationItem, completion: ((_ success: Bool, _ error: Error?) -> Void)?)
}

struct FirebaseExpirationItemModel: ExpirationItemModel {
    
    fileprivate let userId: String
    
    init(userId: String) {
        self.userId = userId
    }
    
    func create(newItem item: ExpirationItem, completion: ((ExpirationItem?, Error?) -> Void)?) {
        let ref = Database.database().reference().child(.items(userId: userId)).childByAutoId()
        save(item: item, toRef: ref, completion: completion)
    }
    
    func updateValuesFor(item: ExpirationItem, completion: ((_ expirationItem: ExpirationItem?, _ error: Error?) -> Void)?) {
        let ref = Database.database().reference().child(.items(userId: userId)).child(item.firebaseId)
        save(item: item, toRef: ref, completion: completion)
    }
    
    func delete(item: ExpirationItem, completion: ((_ success: Bool, _ error: Error?) -> Void)?) {
        let ref = Database.database().reference().child(.items(userId: userId)).child(item.firebaseId)
        let reportSuccessWorkItem = DispatchWorkItem { completion?(true, nil) }
        
        ref.removeValue { (error, reference) in
            reportSuccessWorkItem.cancel()
            
            completion?(error == nil, error)
        }
        // if callback doesnt arrive within short time we expect user is offline thus firebase callbacks are not called. Call success callback manualy
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: reportSuccessWorkItem)
    }
    
    private func save(item: ExpirationItem, toRef ref: DatabaseReference, completion: ((_ expirationItem: ExpirationItem?, _ error: Error?) -> Void)?) {
        let itemDict = item.dictValue
        let reportSuccessWorkItem = DispatchWorkItem { completion?(item, nil) }
        
        ref.setValue(itemDict) { (error, reference) in
            reportSuccessWorkItem.cancel()
            
            if let error = error {
                completion?(nil, error)
            } else {
                completion?(item, nil)
            }
        }
        // if callback doesnt arrive within short time we expect user is offline thus firebase callbacks are not called. Call success callback manualy
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: reportSuccessWorkItem)
    }
}

struct FirestoreExpirationItemModel: ExpirationItemModel {
    
    fileprivate let userId: String
    
    private var reachability: Reachability = {
        let reachability = try! Reachability()
        return reachability
    }()
    
    init(userId: String) {
        self.userId = userId
    }
    
    func create(newItem item: ExpirationItem, completion: ((ExpirationItem?, Error?) -> Void)?) {
        let data = item.dictValue
        let reportSuccessWorkItem = DispatchWorkItem { completion?(item, nil) }
        
        Firestore.firestore().collection("users").document(userId).collection("items").addDocument(data: data) { error in
            reportSuccessWorkItem.cancel()
            completion?(error == nil ? item : nil, error)
        }
        // if callback doesnt arrive within short time we expect user is offline thus firestore callbacks are not called. Call success callback manualy
        if reachability.connection == .unavailable {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: reportSuccessWorkItem)
        }
    }
    
    func updateValuesFor(item: ExpirationItem, completion: ((_ expirationItem: ExpirationItem?, _ error: Error?) -> Void)?) {
        let data = item.dictValue
        let reportSuccessWorkItem = DispatchWorkItem { completion?(item, nil) }
        
        Firestore.firestore().collection("users").document(userId).collection("items").document(item.firebaseId).setData(data) { error in
            reportSuccessWorkItem.cancel()
            completion?(error == nil ? item : nil, error)
        }
        // if callback doesnt arrive within short time we expect user is offline thus firestore callbacks are not called. Call success callback manualy
        if reachability.connection == .unavailable {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: reportSuccessWorkItem)
        }
    }
    
    func delete(item: ExpirationItem, completion: ((_ success: Bool, _ error: Error?) -> Void)?) {
        let reportSuccessWorkItem = DispatchWorkItem { completion?(true, nil) }
        
        Firestore.firestore().collection("users").document(userId).collection("items").document(item.firebaseId).delete { error in
            reportSuccessWorkItem.cancel()
            completion?(error == nil, error)
        }
        // if callback doesnt arrive within short time we expect user is offline thus firestore callbacks are not called. Call success callback manualy
        if reachability.connection == .unavailable {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: reportSuccessWorkItem)
        }
    }
}
