//
//  ObjC.h
//  Expires 3
//
//  Created by Jiri Urbasek on 27/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjC : NSObject

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end
