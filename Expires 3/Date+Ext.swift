//
//  Additions.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 26/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation

extension Date {
    func rounded(toHour hour: Int, minute: Int? = nil, second: Int? = nil) -> Date {
        
        var currentCalendar = NSCalendar.autoupdatingCurrent
        let currentDateComponents = currentCalendar.dateComponents([.year, .month, .day], from: self)
        
        currentCalendar.timeZone = NSTimeZone.system
        
        var roundedDateComponents = DateComponents()
        roundedDateComponents.year = currentDateComponents.year
        roundedDateComponents.month = currentDateComponents.month
        roundedDateComponents.day = currentDateComponents.day
        roundedDateComponents.hour = hour
        roundedDateComponents.minute = minute ?? 0
        roundedDateComponents.second = second ?? 0
        
        return currentCalendar.date(from: roundedDateComponents) ?? self
    }
 
    func roundedDaysRemining(untilDate date: Date) -> Int {
        var daysRemining = self.rounded(toHour: 23, minute: 59, second: 59).timeIntervalSince(date) / (60 * 60 * 24)
        
        if daysRemining < 0 {
            daysRemining -= 1
        }
        return Int(daysRemining)
    }
}
