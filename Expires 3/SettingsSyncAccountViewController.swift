    //
//  SettingsSyncAccountViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 19/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

protocol SettingsSyncAccountViewControllerDelegate {
    func signInButtonTouched()
    func singInDifferentAccountButtonTouched()
}
    
class SettingsSyncAccountViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var signInButton: ColoredButton!
    @IBOutlet weak var signOutButton: ColoredButton!
    
    var viewModel: UserAuthViewModelType! {
        didSet {
            if isViewLoaded { refreshUI() }
        }
    }
    
    var delegate: SettingsSyncAccountViewControllerDelegate?
    
    static func create(with storyboard: UIStoryboard, viewModel: UserAuthViewModelType) -> SettingsSyncAccountViewController {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "SettingsSyncAccountViewController") as! SettingsSyncAccountViewController
        viewController.viewModel = viewModel
        
        return viewController
    }
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .expLightLightViolet
        
        [signInButton, signOutButton].forEach {
            $0?.applyRoundedStyle(for: UIColor.expViolet.withAlphaComponent(0.9), borderColor: .expViolet2)
            $0?.setTitleColor(.expLightLightViolet, for: .normal)
        }
        
        statusLabel.textColor = .expViolet2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        refreshUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshUI() {
        statusLabel.text = viewModel.authStatusText
        
        if viewModel.isUserSignedIn {
            signOutButton.isHidden = false
            signInButton.isHidden = true
        } else {
            signOutButton.isHidden = true
            signInButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshUI()
    }
    
    // MARK: Action

    @IBAction func createAccountButtonTouched(_ sender: Any) {
        delegate?.signInButtonTouched()
    }
    
    @IBAction func signOutButtonTouched(_ sender: Any) {
        delegate?.singInDifferentAccountButtonTouched()
    }
}

