//
//  SettingsAlertTimeViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 21/04/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit


protocol SettingsAlertTimeViewControllerDelegate {
    func controller(_ controller: SettingsAlertTimeViewController, didChangeDate newDate: Date)
}

class SettingsAlertTimeViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alertTimeLabel: UILabel!
    @IBOutlet weak var alertTimePicker: UIDatePicker!
    
    var initialDate = Date().rounded(toHour: 8, minute: 0)
    
    var delegate: SettingsAlertTimeViewControllerDelegate?
    
    
    private var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        return dateFormatter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = NSLocalizedString("Set notification time to", comment: "")
        alertTimePicker.date = initialDate
        alertTimeLabel.text = dateFormatter.string(from: alertTimePicker.date)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .settingsAlertTimeScreenShown)
    }
    
    @IBAction func alertTimePickerValueChanged(_ sender: UIDatePicker) {
        let date = sender.date
        
        alertTimeLabel.text = dateFormatter.string(from: date)
        
        delegate?.controller(self, didChangeDate: date)
    }

}
