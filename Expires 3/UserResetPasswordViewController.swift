//
//  UserResetPasswordViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 19/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

class UserResetPasswordViewController: UIViewController {

    @IBOutlet weak var resetButton: ColoredButton!
    
    var viewModel: UserResetPasswordViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("Reset password", comment: "")
        
        resetButton.layer.borderWidth = 0.5
        resetButton.layer.cornerRadius = 8
        resetButton.tintColor = UIColor.expViolet
        resetButton.setBackgroundColor(color: UIColor.clear, forState: .normal)
        resetButton.setBorderColor(color: UIColor.expViolet, forState: .normal)
        resetButton.setBorderColor(color: UIColor.black.withAlphaComponent(0.4), forState: .disabled)
        resetButton.isEnabled = false
        
        viewModel.canReset.bindAndFire { (canReset) in
            self.resetButton.isEnabled = canReset
        }
    }

    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        viewModel.email = textField.text ?? ""
    }
    
    @IBAction func resetButtonTouched(_ sender: Any) {
        viewModel.reset()
    }
}

extension UserResetPasswordViewController {
    static func create(with storyboard: UIStoryboard, viewModel: UserResetPasswordViewModel) -> UserResetPasswordViewController {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "UserResetPasswordViewController") as! UserResetPasswordViewController
        viewController.viewModel = viewModel
        
        return viewController
    }
}
