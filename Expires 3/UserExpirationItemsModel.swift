//
//  UserExpirationItemsModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 19/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseFirestore

enum LoadingState {
    case notStarted, inProgress, finished
}

enum DataOrigin {
    case cache, remote
}

protocol UserExpirationItemsModelType {
    init(userId: String)
    
    var items: Dynamic<[ExpirationItem]?> { get }
    
    var didAddItem: ((_ item: ExpirationItem) -> Void)? { get set }
    var didRemoveItem: ((_ item: ExpirationItem) -> Void)? { get set }
    var didChangeItem: ((_ item: ExpirationItem) -> Void)? { get set }
    
    var loadingError: Dynamic<Error?> { get }
    
    func load(completion: ((_ items: [ExpirationItem]?, _ error: Error?) -> Void)?)
    
}

class FirebaseExpirationItemsModel: UserExpirationItemsModelType {
    
    private var ref: DatabaseReference
    private var handles = [DatabaseHandle]()
    
    required init(userId: String) {
        ref = Database.database().reference().child(.items(userId: userId))
    }
    
    deinit {
        detachDataObservers()
    }
    
    var items: Dynamic<[ExpirationItem]?> = Dynamic(nil)
    
    var didAddItem: ((ExpirationItem) -> Void)?
    var didRemoveItem: ((ExpirationItem) -> Void)?
    var didChangeItem: ((ExpirationItem) -> Void)?
    
    var loadingError: Dynamic<Error?> = Dynamic(nil)
    
    func load(completion: (([ExpirationItem]?, Error?) -> Void)?) {
        detachDataObservers()
        
        var isFirstFetch = true
        handles.append(ref.observe(.value, with: { (snapshot) in
            
            var items = [ExpirationItem]()
            
            if let dict = snapshot.value as? [String : AnyObject] {
                let array = Array(dict.keys)
                
                for itemId in array {
                    if let itemDict = dict[itemId] as? JSONDict, let item = ExpirationItem(id: itemId, dictionary: itemDict) {
                        items.append(item)
                    }
                }
                items.sort(by: { $0.expirationDate.timeIntervalSince($1.expirationDate) < 0 })
            }
            DispatchQueue.main.async {
                self.items.value = items
                
                if isFirstFetch {
                    completion?(items, nil)
                    isFirstFetch = false
                }
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.loadingError.value = error
                
                if isFirstFetch {
                    completion?(nil, error)
                    isFirstFetch = false
                }
            }
        })
        handles.append(ref.observe(.childAdded, with: { (snapshot) -> Void in
            guard !isFirstFetch else { return }
            
            let itemId = snapshot.key
            if let dict = snapshot.value as? [String : AnyObject], let item = ExpirationItem(id: itemId, dictionary: dict) {
                DispatchQueue.main.async {
                    self.didAddItem?(item)
                    self.items.value?.append(item)
                }
            }
        }))
        handles.append(ref.observe(.childRemoved, with: { (snapshot) -> Void in
            let itemId = snapshot.key
            if let dict = snapshot.value as? [String : AnyObject], let item = ExpirationItem(id: itemId, dictionary: dict) {
                DispatchQueue.main.async {
                    self.didRemoveItem?(item)
                    if let idx = self.items.value?.firstIndex(of: item) {
                        self.items.value?.remove(at: idx)
                    }
                }
            }
        }))
        handles.append(ref.observe(.childChanged, with: { (snapshot) -> Void in
            let itemId = snapshot.key
            if let dict = snapshot.value as? [String : AnyObject], let item = ExpirationItem(id: itemId, dictionary: dict) {
                DispatchQueue.main.async {
                    self.didChangeItem?(item)
                    if let idx = self.items.value?.firstIndex(of: item) {
                        self.items.value?[idx] = item
                    }
                }
            }
        }))
    }
    
    func item(at index: Int) -> ExpirationItem? {
        guard let expirationItems = items.value, expirationItems.count > index else {
            return nil
        }
        return expirationItems[index]
    }
    
    private func detachDataObservers() {
        ref.removeAllObservers()
        handles.removeAll()
    }
}

class FirestoreExpirationItemsModel: UserExpirationItemsModelType {
    
    private var ref: CollectionReference
    private var listener: ListenerRegistration?
    
    required init(userId: String) {
        ref = Firestore.firestore().collection("users").document(userId).collection("items")
    }
    
    deinit {
        detachListeners()
    }
    
    var items: Dynamic<[ExpirationItem]?> = Dynamic(nil)
    
    var didAddItem: ((ExpirationItem) -> Void)?
    var didRemoveItem: ((ExpirationItem) -> Void)?
    var didChangeItem: ((ExpirationItem) -> Void)?
    
    var loadingError: Dynamic<Error?> = Dynamic(nil)
    
    func load(completion: (([ExpirationItem]?, Error?) -> Void)?) {
        detachListeners()
        
        var isFirstFetch = true
        listener = ref.addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot else {
                DispatchQueue.main.async {
                    completion?(nil, error)
                    isFirstFetch = false
                    self.loadingError.value = error
                }
                return
            }
            var items = [ExpirationItem]()
            snapshot.documents.forEach {
                guard let item = ExpirationItem(id: $0.documentID, dictionary: $0.data()) else { return }
                items.append(item)
            }
            if isFirstFetch {
                completion?(items, nil)
                isFirstFetch = false
            } else {
                snapshot.documentChanges.forEach { diff in
                    guard let item = ExpirationItem(id: diff.document.documentID, dictionary: diff.document.data()) else { return }
                        
                    DispatchQueue.main.async {
                        switch diff.type {
                        case .added:
                            self.didAddItem?(item)
                        case .removed:
                            self.didRemoveItem?(item)
                        case .modified:
                            self.didChangeItem?(item)
                        }
                    }
                }
            }
            self.items.value = items
        }
    }
    
    fileprivate func detachListeners() {
        listener?.remove()
    }
}
