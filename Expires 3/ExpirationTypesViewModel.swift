//
//  ExpirationTypesViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 24/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth

struct RowConfiguration {
    let title: String
    let accessory: UITableViewCell.AccessoryType
}

protocol ExpirationTypesViewModel {
    var reminderTypesManager: ReminderTypesManager? { get }
    
    var isEditingEnabled: Dynamic<Bool> { get }
    var didUpdateTypes: (() -> Void)? { get set }
    
    var rowsCount: Int { get }
    func rowConfiguration(at indexPath: IndexPath) -> RowConfiguration
    func canEditType(at indexPath: IndexPath) -> Bool
    func canMoveType(from indexPath: IndexPath) -> Bool
    func canMoveType(to indexPath: IndexPath) -> Bool
    func moveType(at sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    func deleteType(at indexPath: IndexPath)
    
    var typesLoadError: Dynamic<Error?> { get }
    func load()
}

class ExpirationTypesViewModelImpl: ExpirationTypesViewModel {
    
    fileprivate var privateTypes: [String]? {
        didSet {
            isEditingEnabled.value = privateTypes?.isEmpty == false
        }
    }
    
    fileprivate var publicTypes: [String]?
    fileprivate var types: [String]? {
        return privateTypes ?? publicTypes
    }
    
    private(set) var reminderTypesManager: ReminderTypesManager?
    
    var isEditingEnabled = Dynamic(false)
    
    var didUpdateTypes: (() -> Void)?
    
    var typesLoadError: Dynamic<Error?> = Dynamic(nil)
    
    var rowsCount: Int {
        return (types?.count ?? 0) + 1
    }
    
    init() {
    }
    
    func rowConfiguration(at indexPath: IndexPath) -> RowConfiguration {
        
        if indexPath.row == 0 {
            return RowConfiguration(title: NSLocalizedString("... Other", comment: ""), accessory: .none)
        }
        
        let index = indexPath.row - 1
        guard let types = types, types.count > index else {
            return RowConfiguration(title: "", accessory: .none)
        }
        return RowConfiguration(title: types[index], accessory: .none)
    }
    
    func canEditType(at indexPath: IndexPath) -> Bool {
        return isEditingEnabled.value && indexPath.row > 0
    }
    
    func canMoveType(from indexPath: IndexPath) -> Bool {
        return isEditingEnabled.value && indexPath.row > 0
    }
    
    func canMoveType(to indexPath: IndexPath) -> Bool {
        return isEditingEnabled.value && indexPath.row > 0
    }
    
    func moveType(at sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard isEditingEnabled.value, sourceIndexPath.row > 0, destinationIndexPath.row > 0 else { return }
        
        if let type = privateTypes?[sourceIndexPath.row - 1], let index = privateTypes?.firstIndex(of: type), let removedType = privateTypes?.remove(at: index) {
            let newIdx = destinationIndexPath.row - 1
            privateTypes?.insert(removedType, at: newIdx)
            reminderTypesManager?.move(privateType: removedType, toOrder: newIdx)
        }
    }
    
    func deleteType(at indexPath: IndexPath) {
        guard isEditingEnabled.value, indexPath.row > 0 else { return }
        
        if let type = privateTypes?[indexPath.row - 1], let index = privateTypes?.firstIndex(of: type), let removedType = privateTypes?.remove(at: index) {
            reminderTypesManager?.delete(privateType: removedType)
        }
    }
    
    func load() {
        func recreateTypesManager() {
            self.reminderTypesManager = AppState.shared.reminderTypesManager
            self.reminderTypesManager?.delegate = self
        }
        recreateTypesManager()
        
        NotificationCenter.default.addObserver(forName: .authStateChanged, object: nil, queue: nil) { _ in
            recreateTypesManager()
        }
        reminderTypesManager?.observePrivateTypes()
        
        let defaultLangCode = "en"
        let deviceLangCode = Locale.current.languageCode ?? defaultLangCode
        
        reminderTypesManager?.loadPublicTypes(forLanguage: deviceLangCode)
    }
}

extension ExpirationTypesViewModelImpl: ReminderTypesManagerDelegate {
    
    func manager(_ manager: ReminderTypesManager, didLoadPublicTypes types: [String], forLanguage languageCode: String) {
        publicTypes = types
        didUpdateTypes?()
    }
    
    func manager(_ manager: ReminderTypesManager, didUpdatePrivateTypes types: [String]?) {
        privateTypes = types
        didUpdateTypes?()
    }
    
    func manager(_ manager: ReminderTypesManager, didReceiveError error: Error) {
        typesLoadError.value = error
        
        privateTypes = nil
        publicTypes = nil
        
        didUpdateTypes?()
    }
}
