//
//  UserAuthCoordinator.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

class UserAuthCoordinator: Coordinator {
    
    internal var coordinators: [Coordinator] = []
    
    fileprivate var navigationController: UINavigationController
    fileprivate var viewController: SettingsSyncAccountViewController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        viewController = SettingsSyncAccountViewController.create(with: Storyboard.main, viewModel: UserAuthViewModel())
    }
    
    func start() {
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
}

extension UserAuthCoordinator: SettingsSyncAccountViewControllerDelegate {
    func signInButtonTouched() {
        let signInCoordinator = SignInCoordinator(navigationController: navigationController)
        signInCoordinator.delegate = self
        signInCoordinator.start()
        addCoordinator(signInCoordinator)
    }
    
    func singInDifferentAccountButtonTouched() {
        let signInCoordinator = SignInCoordinator(navigationController: navigationController)
        signInCoordinator.delegate = self
        signInCoordinator.start()
        addCoordinator(signInCoordinator)
    }
}

extension UserAuthCoordinator: SignInCoordinatorDelegate {
    func coordinatorDidAuthenticate(coordinator: SignInCoordinator) {
        
        viewController.viewModel = UserAuthViewModel()
        
        coordinator.stop()
        removeCoordinator(coordinator)
    }
    
    func coordinator(coordinator: SignInCoordinator, didCancelWith error: Error?) {
        coordinator.stop()
        removeCoordinator(coordinator)
    }
}
