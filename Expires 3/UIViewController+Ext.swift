//
//  UIViewController+Ext.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/04/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

extension UIViewController {
    @objc func present(error: Error) {
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        present(alertController, animated: true, completion: nil)
    }
}
