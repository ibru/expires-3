//
//  UserSignInViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit
import AuthenticationServices
import FBSDKLoginKit

protocol UserSignInViewControllerDellegate: class {
    func viewController(_ viewController: UserSignInViewController, didChooseSignInMethod method: UserSignInViewController.SignInMethod)
    func viewControllerWantsResetEmail()
    func viewControllerDidAuthenticate(_ viewController: UserSignInViewController)
}

class UserSignInViewController: UIViewController {
    
    enum SignInMethod {
        case twitter
        case facebook
        case apple
    }
    
    weak var delegate: UserSignInViewControllerDellegate?
    
    var viewModel: UserSignInViewModelType!

    
    @IBOutlet weak var signInRegisterSegmentedControl: UISegmentedControl!
    @IBOutlet weak var credentialsSignInButton: ColoredButton!
    @IBOutlet weak var formFullNameSectionView: UIStackView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signInProvidersStackView: UIStackView!
    @IBOutlet weak var fbSignInButton: ColoredButton!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var orLabel: UILabel!
    
    @IBOutlet var allFields: [UITextField]!
    
    fileprivate var shouldCreateNewAccount = true
    
    override func loadView() {
        super.loadView()
        
        [fullNameLabel, emailLabel, passwordLabel, orLabel].forEach {
            $0.textColor = .expViolet2
        }
        allFields.forEach {
            $0.textColor = .expViolet2
        }
        signInRegisterSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.expViolet2], for: .normal)
        
        view.backgroundColor = .expLightLightViolet
        
        credentialsSignInButton.applyRoundedStyle(for: UIColor.expViolet.withAlphaComponent(0.9), borderColor: .expViolet2)
        credentialsSignInButton.setTitleColor(.expLightLightViolet, for: .normal)
        credentialsSignInButton.isEnabled = false
        
        fbSignInButton.applyRoundedStyle(for: .expLightLightViolet, borderColor: .expViolet2)
        fbSignInButton.setImage(FBLoginButton().currentImage, for: .normal)
        fbSignInButton.tintColor = .expViolet2
        fbSignInButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 12)
        
        if #available(iOS 13.0, *) {
            let signInWithAppleButton = ASAuthorizationAppleIDButton()
            signInWithAppleButton.addTarget(self, action: #selector(signInWithAppleTouched), for: .touchUpInside)
            signInWithAppleButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            signInProvidersStackView.addArrangedSubview(signInWithAppleButton)
        }
        
        signInRegisterSegmentedControl.setTitle(NSLocalizedString("Create new", comment: ""), forSegmentAt: 0)
        signInRegisterSegmentedControl.setTitle(NSLocalizedString("Use existing", comment: ""), forSegmentAt: 1)
        fullNameLabel.text = NSLocalizedString("Full name", comment: "")
        emailLabel.text = NSLocalizedString("Email", comment: "")
        passwordLabel.text = NSLocalizedString("Password", comment: "")
        orLabel.text = NSLocalizedString("or", comment: "")
        credentialsSignInButton.setTitle(NSLocalizedString("Sign in", comment: ""), for: .normal)
        fbSignInButton.setTitle(NSLocalizedString("Sign in with Facebook", comment: ""), for: .normal)
        forgotPasswordButton.setTitle(NSLocalizedString("Forgot password?", comment: ""), for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.tintColor = UIColor.expViolet
        
        shouldCreateNewAccount = true
        updateFormUI(shouldRegister: shouldCreateNewAccount, animated: false)
        forgotPasswordButton.isHidden = shouldCreateNewAccount
    
        viewModel.canSignIn.bindAndFire { (canSignIn) in
            guard !self.shouldCreateNewAccount else { return }
            self.credentialsSignInButton.isEnabled = canSignIn
        }
        viewModel.canRegister.bindAndFire { (canRegister) in
            guard self.shouldCreateNewAccount else { return }
            self.credentialsSignInButton.isEnabled = canRegister
        }
        viewModel.signedInUser.bind { (user) in
            guard user != nil else { return }
            self.delegate?.viewControllerDidAuthenticate(self)
        }
        viewModel.signInError.bind { (error) in
            guard let error = error else { return }
            self.present(error: error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func signInRegisterSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        shouldCreateNewAccount = sender.selectedSegmentIndex == 0
        updateFormUI(shouldRegister: shouldCreateNewAccount, animated: true)
    }
    
    @IBAction func emailFieldEditingChanged(_ sender: UITextField) {
        viewModel.email = sender.text ?? ""
    }
    
    @IBAction func passwordFieldEditingChanged(_ sender: UITextField) {
        viewModel.password = sender.text ?? ""
    }
    
    @IBAction func credentialsButtonTouched(_ sender: Any) {
        if shouldCreateNewAccount {
            viewModel.register()
        } else {
            viewModel.signIn()
        }
    }
    
    @IBAction func facebookButtonTouched(_ sender: Any) {
        delegate?.viewController(self, didChooseSignInMethod: .facebook)
    }
        
    @IBAction func forgotPasswordButtonTouched(_ sender: Any) {
        delegate?.viewControllerWantsResetEmail()
    }
    
    @objc func signInWithAppleTouched() {
        delegate?.viewController(self, didChooseSignInMethod: .apple)
    }
    
    private func updateFormUI(shouldRegister: Bool, animated: Bool) {
        credentialsSignInButton.setTitle(shouldRegister ? NSLocalizedString("Create account", comment: "") : NSLocalizedString("Sign in", comment: ""), for: .normal)
        
        
        let duration: TimeInterval = animated ? 0.3 : 0
        
        UIView.animate(withDuration: duration) {
            self.formFullNameSectionView.isHidden = !shouldRegister
            self.forgotPasswordButton.isHidden = shouldRegister
        }
    }
    
}

extension UserSignInViewController {
    static func create(with storyboard: UIStoryboard, viewModel: UserSignInViewModelType) -> UserSignInViewController {
        let viewController = Storyboard.main.instantiateViewController(withIdentifier: "UserSignInViewController") as! UserSignInViewController
        viewController.viewModel = viewModel
        
        return viewController
    }
}
