//
//  UserAuthViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol UserAuthViewModelType {
    var userModel: UserAuthModel { get }
    
    var isUserSignedIn: Bool { get }
    
    var userName: String? { get }
    var userEmail: String? { get }
    var authStatusText: String { get }
}

class UserAuthViewModel: UserAuthViewModelType {
    let userModel: UserAuthModel = AppState.shared.modelFactory.userAuthModel()
    
    var isUserSignedIn: Bool {
        guard let currentUser = userModel.currentUser else { return false }
        
        return !currentUser.isAnonymous
    }
    var userName: String? { return userModel.currentUser?.name }
    var userEmail: String? { return userModel.currentUser?.email }
    var authStatusText: String {
        guard isUserSignedIn else {
            return NSLocalizedString("Not signed in", comment: "")
        }
        
        var service: String = ""
        switch userModel.currentUser?.signInService {
        case .password: service = NSLocalizedString("Email", comment: "")
        case .facebook: service = NSLocalizedString("Facebook", comment: "")
        case .apple: service = NSLocalizedString("Apple ID", comment: "")
        default: break
        }
        
        if let identification = userEmail ?? userName {
            return (service.isEmpty ? "" : service + ": ") + identification
        }
        return NSLocalizedString("Signed in", comment: "")
    }
}
