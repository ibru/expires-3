//
//  XIBLocalizable.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 14/01/2020.
//  Copyright © 2020 Jiri Urbasek. All rights reserved.
//

import UIKit

protocol Localizable {
    var localized: String { get }
}
extension String: Localizable {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

protocol XIBLocalizable {
    var localizedKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable var localizedKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }
}
extension UIButton: XIBLocalizable {
    @IBInspectable var localizedKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
   }
}
