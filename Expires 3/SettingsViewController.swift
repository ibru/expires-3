//
//  SettingsViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 18/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

protocol SettingsViewControllerDelegate: class {
    func controller(_ controller: SettingsViewController, didUpdateAlertTime hour: Int, minute: Int)
    func controllerWantsUpgradeToPRO(_ controller: SettingsViewController)
    func controllerWantsRestorePurchases(_ controller: SettingsViewController)
    func controllerWantsRateOnAppStore(_ controller: SettingsViewController)
    func controllerWantsSendFeedback(_ controller: SettingsViewController)
}

class SettingsViewController: UITableViewController, UserDisplayable {
    
    weak var delegate: SettingsViewControllerDelegate?
    
    lazy var userAuthCoordinator: UserAuthCoordinator = { UserAuthCoordinator(navigationController: self.navigationController!) }()
    
    private enum SectionIdx: Int {
        case preferences, purchases, feedback
    }
    private struct RowIdx {
        static let account = 0
        static let alertTime = 1
        
        static let upgrade = 0
        static let restore = 1
        
        static let writeReview = 0
        static let sendFeedback = 1
    }

    override func loadView() {
        super.loadView()
        
        tableView.backgroundColor = .expLightLightViolet
        tableView.separatorColor = .expLightLightViolet
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: .authStateChanged, object: nil, queue: OperationQueue.main) { _ in
            self.tableView.reloadRows(at: [IndexPath(row: RowIdx.account, section: SectionIdx.preferences.rawValue)], with: .automatic)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .settingsScreenShown)
    }
    
    // MARK: Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let idx = SectionIdx(rawValue: section) else { return 0 }
        
        switch idx {
        case .preferences:
            return 2
        case .purchases:
            return 2
        case .feedback:
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let idx = SectionIdx(rawValue: indexPath.section) else { return UITableViewCell() }
        
        let tableCell: UITableViewCell
        switch idx {
        case .preferences:
            if indexPath.row == RowIdx.account {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Right Detail", for: indexPath)
                
                cell.textLabel?.text = NSLocalizedString("Data Sync", comment: "")
                cell.detailTextLabel?.text = NSLocalizedString(AppState.shared.currentUser?.isAnonymous == false ? "Enabled" : "Disabled", comment: "")
                
                tableCell = cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Right Detail", for: indexPath)
                
                cell.textLabel?.text = NSLocalizedString("Alert time", comment: "")
                cell.detailTextLabel?.text = String(format: "%ld:%02ld", UserDefaults.appDefaults.alertTimeHour, UserDefaults.appDefaults.alertTimeMinute)
                
                tableCell = cell
            }
        case .purchases:
            if indexPath.row == RowIdx.upgrade {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Normal", for: indexPath)
                
                cell.textLabel?.text =
                    AppState.shared.enabledFeatures.contains(.unlimitedItemsCount) ?
                    NSLocalizedString("You've upgraded to Expires PRO", comment: "") :
                    NSLocalizedString("Upgrade to Expires PRO", comment: "")
                
                tableCell = cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Normal", for: indexPath)
                
                cell.textLabel?.text = NSLocalizedString("Restore Purchases", comment: "")
                
                tableCell = cell
            }
            
        case .feedback:
            if indexPath.row == RowIdx.writeReview {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Normal", for: indexPath)

                cell.textLabel?.text = NSLocalizedString("Rate us on AppStore", comment: "")
                
                tableCell = cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Settings Cell Normal", for: indexPath)
                
                cell.textLabel?.text = NSLocalizedString("Send us Feedback", comment: "")
                
                tableCell = cell
            }
        }
        
        tableCell.textLabel?.textColor = .expViolet2
        tableCell.detailTextLabel?.textColor = .expViolet2
        
        return tableCell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let idx = SectionIdx(rawValue: indexPath.section) else { return }
        
        switch idx {
        case .preferences:
            if indexPath.row == RowIdx.account {
                userAuthCoordinator.start()
            }
            else if indexPath.row == RowIdx.alertTime {
                let viewController = storyboard?.instantiateViewController(withIdentifier: "SettingsAlertTimeViewController") as! SettingsAlertTimeViewController
                viewController.delegate = self
                viewController.initialDate = Date().rounded(toHour: UserDefaults.appDefaults.alertTimeHour, minute: UserDefaults.appDefaults.alertTimeMinute)
                
                navigationController?.pushViewController(viewController, animated: true)
            }
        case .purchases:
            if indexPath.row == RowIdx.upgrade {
                delegate?.controllerWantsUpgradeToPRO(self)
            } else {
                appAnalytics.log(event: .settingsScreenRestorePurchasesTouched)
                delegate?.controllerWantsRestorePurchases(self)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        case .feedback:
            if indexPath.row == RowIdx.writeReview {
                appAnalytics.log(event: .settingsScreenRateOnAppStoreTouched)
                delegate?.controllerWantsRateOnAppStore(self)
            } else {
                appAnalytics.log(event: .settingsScreenSendFeedbackTouched)
                delegate?.controllerWantsSendFeedback(self)
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: Actions
    
    @IBAction func doneButtonTouched(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Public
    
    func presentAccountScreen() {
        if self.isViewLoaded {
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            self.tableView.delegate?.tableView?(self.tableView, didSelectRowAt: indexPath)
        }
    }
}

extension SettingsViewController: SettingsAlertTimeViewControllerDelegate {
    func controller(_ controller: SettingsAlertTimeViewController, didChangeDate newDate: Date) {
        let currentCalendar = NSCalendar.autoupdatingCurrent
        let dateComponents = currentCalendar.dateComponents([.hour, .minute], from: newDate)
        
        UserDefaults.appDefaults.alertTimeHour = dateComponents.hour ?? 8
        UserDefaults.appDefaults.alertTimeMinute = dateComponents.minute ?? 0
        
        tableView.reloadRows(at: [IndexPath(row: RowIdx.alertTime, section: 0)], with: .automatic)
        
        delegate?.controller(self, didUpdateAlertTime: UserDefaults.appDefaults.alertTimeHour, minute: UserDefaults.appDefaults.alertTimeMinute)
    }
}
