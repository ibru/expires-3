//
//  Analytics.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 14/01/2020.
//  Copyright © 2020 Jiri Urbasek. All rights reserved.
//

import Foundation
import Amplitude_iOS

public let appAnalytics: Analytics = AmplitudeAnalytics()

public enum AnalyticsEvent: String {
    
    case itemsListScreenShown
    case itemsListScreenAddItemButtonTouched
    case itemsListScreenEmptyViewEnableSyncButtonTouched
    case itemsListScreenSettingsButtonTouched
    case itemsListScreenPurchasePromptFooterUpgradeButtonTouched
    
    case itemDetailScreenShown
    
    case expirationTypesListScreenShown
    case expirationTypesListScreenCanceled
    case expirationTypesListScreenDidDeleteItem
    
    case createExpirationItemScreenShown
    case createExpirationItemScreenCreateItemButtonTouched
    
    case reminderTimeScreenShown
    case reminderTimeScreenNotifSettingsNotDeterminedAlertShown
    case reminderTimeScreenNotifSettingsDeniedAlertShown
    case reminderTimeScreenNotifSettingsAlertDoActionTouched
    case reminderTimeScreenNotifSettingsAlertNotNowTouched
    
    case settingsScreenShown
    case settingsScreenRestorePurchasesTouched
    case settingsScreenRateOnAppStoreTouched
    case settingsScreenSendFeedbackTouched
    
    case settingsAlertTimeScreenShown
    
    case signedInUserAccount
    
    case proUpgradeScreenShown
    case proUpgradeScreenUpgradeButtonTouched
}

public protocol Analytics {
    
    func setup()
    func log(event: AnalyticsEvent, params: [String: Any]?)
}
extension Analytics {
    func log(event: AnalyticsEvent, params: [String: Any]? = nil) {
        log(event: event, params: params)
    }
}


public struct AmplitudeAnalytics: Analytics {
    
    public func setup() {
        Amplitude.instance()?.initializeApiKey("ebf65d5ad0860d99a94b074e0edaf392")
    }
    
    public func log(event: AnalyticsEvent, params: [String: Any]?) {
        #if DEBUG
        print("Analytics event: \(event.rawValue), params: \(String(describing: params))")
        #endif
        if let params = params {
            Amplitude.instance()?.logEvent(event.rawValue, withEventProperties: params)
        } else {
            Amplitude.instance()?.logEvent(event.rawValue)
        }
    }
}
