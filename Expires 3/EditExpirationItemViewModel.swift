//
//  EditExpirationItemViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 29/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation


protocol EditExpirationItemViewModelDelegate: class {
    func modelDidUpdateItem(_ model: EditExpirationItemViewModel)
    func modelDidDeleteItem(_ model: EditExpirationItemViewModel)
}

protocol EditExpirationItemViewModel {
    var delegate: EditExpirationItemViewModelDelegate? { get set }
    
    var expirationItem: ExpirationItem? { get set }
    
    var itemType: String? { get set }
    var expirationDate: Date? { get set }
    var reminderIntervals: [Int]? { get set }
    var notes: String? { get set }
    
    var isEmpty: Dynamic<Bool> { get }
    var canUpdateItem: Dynamic<Bool> { get }
    var canDeleteItem: Dynamic<Bool> { get }
    var itemUpdateError: Dynamic<Error?> { get }
    func updateItem()
    func deleteItem()
    func test_PostNotification()
}

class ItemDetailViewModel: EditExpirationItemViewModel {
    lazy fileprivate var authModel: UserAuthModel = AppState.shared.modelFactory.userAuthModel()
    
    var delegate: EditExpirationItemViewModelDelegate?
    
    var expirationItem: ExpirationItem? {
        didSet {
            itemType = expirationItem?.title
            expirationDate = expirationItem?.expirationDate
            reminderIntervals = expirationItem?.reminders
            notes = expirationItem?.notes
            
            isEmpty.value = expirationItem == nil
        }
    }
    
    var itemType: String? {
        didSet {
            checkIfCanEditItem()
        }
    }
    
    var expirationDate: Date? {
        didSet {
            checkIfCanEditItem()
        }
    }
    
    var reminderIntervals: [Int]? {
        didSet {
            checkIfCanEditItem()
        }
    }
    
    var notes: String? {
        didSet {
            checkIfCanEditItem()
        }
    }
    
    var isEmpty = Dynamic(true)
    
    var canUpdateItem = Dynamic(false)
    
    var canDeleteItem = Dynamic(false)
    
    var itemUpdateError = Dynamic<Error?>(nil)
    
    var itemsProvider: ExpirationItemsProvider
    
    init(itemsProvider: ExpirationItemsProvider) {
        self.itemsProvider = itemsProvider
    }
    
    func updateItem() {
        guard canUpdateItem.value, let itemId = expirationItem?.firebaseId, let title = itemType, let date = expirationDate else { return }
        
        let createdExpirationItem = ExpirationItem(id: itemId, title: title, date: date, reminders: reminderIntervals, notes: notes)
        
        guard let user = authModel.currentUser else {
            authModel.signInAnonymously(completion: { (user, error) in
                if let user = user {
                    self.save(updatedItem: createdExpirationItem, forUser: user)
                } else {
                    self.itemUpdateError.value = error
                }
            })
            return
        }
        save(updatedItem: createdExpirationItem, forUser: user)
    }
    
    func deleteItem() {
        guard let user = authModel.currentUser else {
            //TODO: Report error user not authenticated
            return
        }
        guard canDeleteItem.value, let item = expirationItem else { return }
        
        let itemModel: ExpirationItemModel = AppState.shared.modelFactory.expirationItemModel(userId: user.id)
        itemModel.delete(item: item) { (success, error) in
            if success {
                NotificationsManager.removeNotifications(forItem: item) { }
                self.expirationItem = nil
                self.delegate?.modelDidDeleteItem(self)
            } else {
                self.itemUpdateError.value = error
            }
        }
    }
    
    private func checkIfCanEditItem() {
        canUpdateItem.value = expirationItem != nil && itemType != nil && itemType?.isEmpty == false && expirationDate != nil
        canDeleteItem.value = expirationItem?.firebaseId != nil
    }
    
    private func save(updatedItem item: ExpirationItem, forUser user: DatabaseUser) {
        let itemModel: ExpirationItemModel = AppState.shared.modelFactory.expirationItemModel(userId: user.id)
        itemModel.updateValuesFor(item: item) { (item, error) in
            if let item = item {
                self.expirationItem = item
                self.delegate?.modelDidUpdateItem(self)
            } else {
                self.itemUpdateError.value = error
            }
        }
    }
    
    func test_PostNotification() {
        guard let item = expirationItem else { return }
        NotificationsManager.testScheduleNotification(for: item, fireAfter: 5)
    }
}
