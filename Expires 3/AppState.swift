//
//  AppState.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 20/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

class AppState {
    static let shared = AppState(factory: .firestore)
    
    lazy var reminderTypesManager = modelFactory.reminderTypesManager(userId: modelFactory.userAuthModel().currentUser?.id)
    
    let modelFactory: ModelsFactory
    
    var currentUser: DatabaseUser? {
        didSet {
            NotificationCenter.default.post(name: .authStateChanged, object: self, userInfo: nil)
        }
    }
    var currentUserInfo: UserAdditionalInfo? {
        didSet {
            NotificationCenter.default.post(name: .userAdditionalInfoChanged, object: self, userInfo: nil)
        }
    }
    var enabledFeatures: Set<AppFeature> = []
    
    fileprivate let featuresManager: AppFeaturesManager
    
    private init(factory: ModelsFactory) {
        modelFactory = factory
        let userModel = modelFactory.userAuthModel()
        
        featuresManager = AppFeaturesManager(localDetector: LocalAppFeaturesDetector(validator: IAPManager()),
                                             remoteDetector: RemoteAppFeaturesDetector(iapValidator: IAPManager(), userDataChecker: UserAuthModelFeaturesChecker(model: userModel)))
        refreshEnabledFeatures()
        
        userModel.userAuthChanged = {
            self.currentUser = userModel.currentUser
            self.enabledFeatures = []
            self.refreshEnabledFeatures()
            self.reminderTypesManager = self.modelFactory.reminderTypesManager(userId: userModel.currentUser?.id)
        }
        userModel.userAdditionalInfoChanged = { additionalInfo in
            self.currentUserInfo = additionalInfo
            self.enabledFeatures = []
            self.refreshEnabledFeatures()
        }
        
        NotificationCenter.default.addObserver(forName: .IAPProductWasPurchased, object: nil, queue: OperationQueue.main) { notif in
            self.refreshEnabledFeatures()
        }
    }
    
    fileprivate func refreshEnabledFeatures() {
        featuresManager.detectEnabledFeatures { features, validationType in
            self.enabledFeatures = features 
            NotificationCenter.default.post(name: .enabledAppFeaturesChanged, object: self, userInfo: nil)
        }
    }
}

extension Notification.Name {
    static let authStateChanged = Notification.Name("authStateChanged")
    static let userAdditionalInfoChanged = Notification.Name("userAdditionalInfoChanged")
    static let enabledAppFeaturesChanged = Notification.Name("enabledAppFeaturesChanged")
}
