//
//  User.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth

enum SignInService {
    case none, password, facebook, apple, unknown
}

protocol DatabaseUser: class {
    var id: String { get }
    var name: String? { get }
    var email: String? { get }
    var isAnonymous: Bool { get }
    var signInService: SignInService { get }
}

extension User: DatabaseUser {
    var id: String { return self.uid }
    var name: String? { return self.displayName }
    var isSignedIn: Bool { return !isAnonymous }
    
    var signInService: SignInService {
        if isAnonymous {
            return .none
        }
        if let signInInfo = providerData.first {
            switch signInInfo.providerID {
            case "facebook.com": return .facebook
            case "apple.com": return .apple
            case "password": return .password
            default: return .unknown
            }
        }
        
        return .unknown
    }
}


protocol UserDisplayable {
    func displayName(for user: DatabaseUser) -> String
}
extension UserDisplayable {
    func displayName(for user: DatabaseUser) -> String {
        if user.isAnonymous {
            return NSLocalizedString("Anonymous user", comment: "")
        }
        return user.email ?? user.name ?? NSLocalizedString("Unspecified", comment: "")
    }
}
