//
//  ExpirationItemCellViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 19/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

protocol ExpirationItemCellViewModelType {
    var item: ExpirationItem { get } // for complatibility with old code. But we should use text properties for each label instead
    var index: Int { get }// for complatibility with old code. But we should use text properties for each label instead
}

struct ExpirationItemCellViewModel : ExpirationItemCellViewModelType {
    let item: ExpirationItem
    let index: Int
}
