//
//  ViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import UserNotifications


class ItemsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyViewTitle: UILabel!
    @IBOutlet weak var emptyViewSubtitle: UILabel!
    @IBOutlet weak var emptyViewButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    var viewModel: LimitedUserItemsViewModelType! {
        didSet {
            if isViewLoaded {
                bind(to: &viewModel!)
                viewModel.load()
            }
        }
    }
    
    private lazy var backgroundLayer: CALayer! = {
        
        let topColor = UIColor(red: 33.0/255.0, green: 18.0/255.0, blue: 87.0/255.0, alpha: 1)
        let bottomColor = UIColor(red: 194.0/255.0, green: 189.0/255.0, blue: 229.0/255.0, alpha: 1)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.locations = [0.1, 1.0]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        
        return gradientLayer
    }()
    
    private lazy var loadingActivityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.style = .large
        activityIndicator.color = .expWhite
        return activityIndicator
    }()
    
    private var selectedItem: ExpirationItem?
    
    override func loadView() {
        super.loadView()
        
        splitViewController?.delegate = self
        
        backgroundLayer.frame = view.bounds
        view.layer.insertSublayer(backgroundLayer, at: 0)
        view.tintColor = .expViolet
        
        collectionView.contentInset = UIEdgeInsets.zero
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing = 6
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing = 6
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 55, right: 0)
        
        addButton.setTitle("+ " + NSLocalizedString("Add event", comment: "").uppercased(), for: .normal)
        addButton.tintColor = UIColor.expViolet
        
        settingsButton.tintColor = UIColor.expViolet
        
        emptyViewTitle.text = NSLocalizedString("Feeling empty?", comment: "")
        emptyViewSubtitle.text = NSLocalizedString("Start by adding new item or syncing data from your other device.", comment: "")
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.expWhite,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        emptyViewButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("Enable data synchronization", comment: ""), attributes: attrs), for: .normal)
        emptyViewButton.setTitleColor(.expWhite, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bind(to: &viewModel!)
        viewModel.load()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = CGSize(width: collectionView.bounds.width - 16, height: 72)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        makeNavigationBarTransparent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        appAnalytics.log(event: .itemsListScreenShown)
        
        for indexPath in collectionView.indexPathsForSelectedItems ?? [] {
            collectionView.deselectItem(at: indexPath, animated: true)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        //view.setNeedsUpdateConstraints()
        
        coordinator.animate(alongsideTransition: { (context) in
            
            (self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = CGSize(width: size.width - 16, height: 72)
            self.collectionView?.collectionViewLayout.invalidateLayout()
            //self.view.updateConstraintsIfNeeded()
            
            self.backgroundLayer.bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
            self.backgroundLayer.position = CGPoint(x: self.backgroundLayer.bounds.midX, y: self.backgroundLayer.bounds.midY)
            
        }, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func emptyViewEnableSyncButtonTouched(_ sender: Any) {
        appAnalytics.log(event: .itemsListScreenEmptyViewEnableSyncButtonTouched)
        viewModel.showUserAccountSettings()
    }
    
    @IBAction func settingsButtonTouched(_ sender: Any) {
        appAnalytics.log(event: .itemsListScreenSettingsButtonTouched)
        viewModel.showSettings()
    }
    
    @IBAction func addItemButtonTouched(_ sender: Any) {
        appAnalytics.log(event: .itemsListScreenAddItemButtonTouched)
        viewModel.addNewItem()
    }
    
    @IBAction func purchasePromptFooterUpgradeButtonTouched(_ sender: Any) {
        appAnalytics.log(event: .itemsListScreenPurchasePromptFooterUpgradeButtonTouched)
        viewModel.askToUpdateLimit()
    }
    
    private func highlightItem(atIndex idx: Int) {
        //let cellViewModel = viewModel.itemModel(at: idx)
        //cell.highlightProgress(forViewModel: cellViewModel)
    }
    
    fileprivate func bind(to viewModel: inout LimitedUserItemsViewModelType) {
        viewModel.contentType.bindAndFire { (contentType) in
            switch contentType {
            case .loading:
                self.loadingActivityIndicator.startAnimating()
                self.collectionView.backgroundView = self.loadingActivityIndicator
                
            case .empty:
                // quick fix for emtpy view being shown for split of a second. Because at the start, VM is loaded withnout nill value for UserExpirationItemsModelType because user account state is not yet known.
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                    if self.viewModel.contentType.value == .empty {
                        self.collectionView.backgroundView = self.emptyView
                        self.loadingActivityIndicator.stopAnimating()
                    }
                }
                
            case .itemsList:
                self.collectionView.backgroundView = nil
                self.loadingActivityIndicator.stopAnimating()
            }
        }
        viewModel.didChangeAllItems = {
            self.collectionView.reloadData()
        }
        viewModel.didAddItem = { idx in
            self.collectionView.reloadData()
            
            doOnMainThreadAfter(delay: 0.3) {
                self.highlightItem(atIndex: idx)
            }
        }
        viewModel.didChangeItem = { idx in
            self.collectionView.reloadData()
            
            doOnMainThreadAfter(delay: 0.3) {
                self.highlightItem(atIndex: idx)
            }
        }
        viewModel.didRemoveItem = { idx in
            self.collectionView.reloadData()
        }
        viewModel.hasItemsOverLimit.bindAndFire { (hasItemsOverLimit) in
            (self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.footerReferenceSize = CGSize(width: self.collectionView.frame.width, height: hasItemsOverLimit ? 120 : 0)
            self.collectionView.reloadData()
        }
    }
}

extension ItemsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item Cell", for: indexPath) as! ExpirationItemCell
        
        if let cellViewModel = viewModel.itemModel(at: indexPath.row) {
            cell.configureWith(model: cellViewModel)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.showDetail(forItemAt: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Upgrade prompt footer", for: indexPath) as! UserItemsListLimitExceededFooterView
        footerView.upgradeButton.addTarget(self, action: #selector(purchasePromptFooterUpgradeButtonTouched), for: .touchUpInside)
        
        return footerView
    }
}
    
extension ItemsViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return selectedItem == nil
    }
}
