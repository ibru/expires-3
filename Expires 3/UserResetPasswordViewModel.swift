//
//  UserResetPasswordViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 19/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol UserResetPasswordViewModelDelegate: class {
    func viewModel(viewModel: UserResetPasswordViewModel, didResetEmailWith success: Bool, error: Error?)
}

class UserResetPasswordViewModel {
    var canReset: Dynamic<Bool> = Dynamic(false)
    
    weak var delegate: UserResetPasswordViewModelDelegate?
    
    var email: String = "" {
        didSet {
            canReset.value = isValid(email: email)
        }
    }
    
    func reset() {
        guard canReset.value else { return }
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            self.delegate?.viewModel(viewModel: self, didResetEmailWith: error == nil, error: error)
        }
    }
    
    fileprivate func isValid(email: String) -> Bool {
        guard email.lengthOfBytes(using: .utf8) > 5 else { return false }
        
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
