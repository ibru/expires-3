//
//  ItemSummaryView.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 23/05/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation
import KDCircularProgress


enum ItemSummaryColorStyle {
    case violet
    case red
}

protocol ItemSummaryView {
    var titleLbl: UILabel? { get }
    var dayNumberLbl: UILabel? { get }
    var dayTextLbl: UILabel? { get }
    var dateLabel: UILabel? { get }
    var notesLabel: UILabel? { get }
    var progressBar: KDCircularProgress? { get }
    
    
    func applyStyle(style: ItemSummaryColorStyle)
    
    func configure(withItem item: ExpirationItem, animated: Bool)
    
    func style(forItem item: ExpirationItem) -> ItemSummaryColorStyle
    
    func titleColor(forStyle style: ItemSummaryColorStyle) -> UIColor
    
    func progress(forItem item: ExpirationItem) -> Double
    
    func updateProgress(progress: Double, animated: Bool)
}

extension ItemSummaryView {
    
    func titleColor(forStyle style: ItemSummaryColorStyle) -> UIColor {
        return style == .violet ? UIColor.expViolet : UIColor.expRed
    }
    
    func applyStyle(style: ItemSummaryColorStyle) {
        
        titleLbl?.textColor = titleColor(forStyle: style)
        
        dateLabel?.textColor = style == .violet ? UIColor.expLightViolet : UIColor.expLightRed
        dayNumberLbl?.textColor = style == .violet ? UIColor.expViolet : UIColor.expRed
        dayTextLbl?.textColor = style == .violet ? UIColor.expViolet : UIColor.expRed
        
        progressBar?.startAngle = -90
        progressBar?.glowMode = .noGlow
        progressBar?.trackColor = style == .violet ? UIColor.expLightLightViolet : UIColor.expLightLightRed
        progressBar?.progressColors = style == .violet ? [UIColor.expViolet] : [UIColor.expRed]
        
    }
    
    func style(forItem item: ExpirationItem) -> ItemSummaryColorStyle {
        let daysRemaining = item.expirationDate.roundedDaysRemining(untilDate: Date())
        return daysRemaining < highlightedDaysRemaining ? .red : .violet
    }
    
    func configure(withItem item: ExpirationItem, animated: Bool = false) {
        let daysRemaining = item.expirationDate.roundedDaysRemining(untilDate: Date())
        applyStyle(style: daysRemaining < highlightedDaysRemaining ? .red : .violet)
        
        var daysWord = NSLocalizedString("days5+", comment: "")
        if abs(daysRemaining) < 5 && abs(daysRemaining) > 1 {
            daysWord = NSLocalizedString("days2to4", comment: "")
        }
        else if abs(daysRemaining) == 1 {
            daysWord = NSLocalizedString("days1", comment: "")
        }
        else if abs(daysRemaining) == 0 {
            daysWord = NSLocalizedString("today", comment: "")
        }
        dayTextLbl?.text = daysWord
        dayNumberLbl?.text = "\(daysRemaining)"
        
        titleLbl?.text = item.title
        dateLabel?.text = dateFormatter.string(from: item.expirationDate)
        notesLabel?.text = item.notes
        
        updateProgress(progress: progress(forItem: item), animated: animated)
    }
    
    func progress(forItem item: ExpirationItem) -> Double {
        let daysRemaining = item.expirationDate.roundedDaysRemining(untilDate: Date())
        
        var progress = Double(daysRemaining) / 30.0
        if (progress > 1) {
            progress = 1
        }
        if (progress < -1) {
            progress = -1
        }
        return progress
    }
    
    func updateProgress(progress: Double, animated: Bool) {
        progressBar?.clockwise = progress > 0 ? true : false
        
        let progress = progress > 0 ? progress : -progress
        let finalAngle = 360 * progress
        
        if animated {
            progressBar?.animate(toAngle: finalAngle, duration: 1.5, relativeDuration: false, completion: nil)
        }
        else {
            progressBar?.angle = finalAngle
        }
    }
}
