//
//  ExpirationTypesViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import Firebase


class ExpirationTypesViewController: UITableViewController {
    
    lazy var viewModel: ExpirationTypesViewModel = ExpirationTypesViewModelImpl()
    
    override func loadView() {
        super.loadView()
        
        navigationController?.navigationBar.tintColor = UIColor.expViolet
        
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = blurEffectView
        tableView.separatorStyle = .none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Choose type", comment: "")
        
        makeNavigationBarTransparent()
        hideBackButtonTitleOnNextScreen()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_down"), style: .plain, target: self, action: #selector(cancelButtonTouched))
        
        viewModel.isEditingEnabled.bindAndFire { (enabled) in
            self.navigationItem.setRightBarButton(enabled ? self.editButtonItem : nil, animated: true)
        }
        viewModel.typesLoadError.bindAndFire { (error) in
            guard let error = error else { return }
            self.present(error: error)
        }
        viewModel.didUpdateTypes = {
            self.tableView.reloadData()
        }
        viewModel.load()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .expirationTypesListScreenShown)
    }
    
    @objc func cancelButtonTouched() {
        appAnalytics.log(event: .expirationTypesListScreenCanceled)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

extension ExpirationTypesViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetDelta: CGFloat = 20
        let offset = scrollView.contentOffset.y + 64
        let alpha = offset <= 0 ? 1 : 1 - (offset/offsetDelta)
        
        if (alpha < -2) { // in case of quick movement and value may quickly become lower than zero
            return
        }
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.expViolet.withAlphaComponent(max(alpha, 0))]
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Type Cell", for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.expViolet
        
        let configuration = viewModel.rowConfiguration(at: indexPath)
        cell.textLabel?.text = configuration.title
        cell.accessoryType = configuration.accessory
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "CreateExpirationItemViewController") as! CreateExpirationItemViewController
        viewController.viewModel.reminderTypesManager = viewModel.reminderTypesManager
        
        if indexPath.row > 0 {
            let configuration = viewModel.rowConfiguration(at: indexPath)
            viewController.itemType = configuration.title
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return viewModel.canEditType(at: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return viewModel.canMoveType(from: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return viewModel.canMoveType(to: proposedDestinationIndexPath) ? proposedDestinationIndexPath : sourceIndexPath
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            appAnalytics.log(event: .expirationTypesListScreenDidDeleteItem)
            viewModel.deleteType(at: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        viewModel.moveType(at: sourceIndexPath, to: destinationIndexPath)
    }
}
