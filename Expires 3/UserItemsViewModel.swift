//
//  UserItemsViewModel.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 18/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import Firebase

enum ContentType {
    case itemsList, loading, empty
}

protocol UserItemsViewModelInteractionDelegate: class {
    func viewModel(_ viewModel: UserItemsViewModelType, wantsShowDetailFor itemModel: EditExpirationItemViewModel)
    func viewModelWantsShowSettings(_ viewModel: UserItemsViewModelType)
    func viewModelWantsShowUserAccountSettings(_ viewModel: UserItemsViewModelType)
    func viewModel(_ viewModel: UserItemsViewModelType, wantsShowNotificationsEnablingPrompt needsToGoToSystemSettings: Bool)
    func viewModel(_ viewModel: UserItemsViewModelType, didUpdateItems items: [ExpirationItem])
    func viewModelWantsAddNewItem(_ viewModel: UserItemsViewModelType)
}

protocol LimitedUserItemsViewModelInteractionDelegate: class {
    func viewModelWantsToIncreaseItemsLimit(_ viewModel: LimitedUserItemsViewModelType)
}

protocol UserItemsViewModelType {
    var delegate: UserItemsViewModelInteractionDelegate? { get set }
    
    var contentType: Dynamic<ContentType> { get }
    
    var didAddItem: ((_ atIdx: Int) -> Void)? { get set }
    var didRemoveItem: ((_ atIdx: Int) -> Void)? { get set }
    var didChangeItem: ((_ atIdx: Int) -> Void)? { get set }
    var didChangeAllItems: (() -> Void)? { get set }
    
    var needsNotificationsEnabled: Dynamic<Bool> { get }
    
    var loadingError: Dynamic<Error?> { get }
    
    var itemsCount: Int { get }
    func itemModel(at index: Int) -> ExpirationItemCellViewModelType?
    
    func load()
    
    func showDetail(forItemAt index: Int)
    
    func showSettings()
    
    func showUserAccountSettings()
    
    func addNewItem()
}

protocol LimitedUserItemsViewModelType: UserItemsViewModelType {
    
    var limitDelegate: LimitedUserItemsViewModelInteractionDelegate? { get set }
    
    var hasItemsOverLimit: Dynamic<Bool> { get }

    func updateLimit(limit: Int?)
    
    func askToUpdateLimit()
    
}

class UserItemsViewModel: UserItemsViewModelType {
    
    weak var delegate: UserItemsViewModelInteractionDelegate?
    
    var contentType: Dynamic<ContentType> = Dynamic(.empty)
    
    var didAddItem: ((Int) -> Void)?
    var didRemoveItem: ((Int) -> Void)?
    var didChangeItem: ((Int) -> Void)?
    var didChangeAllItems: (() -> Void)?
    
    var loadingError: Dynamic<Error?> = Dynamic(nil)
    
    var needsNotificationsEnabled: Dynamic<Bool> = Dynamic(false)
    
    var itemsCount: Int { items.count }
    
    fileprivate var model: UserExpirationItemsModelType?
    
    fileprivate var items = [ExpirationItem]() {
        didSet {
            checkNotificationsNeeded()
        }
    }
    
    init(model: UserExpirationItemsModelType? = nil) {
        self.model = model
        self.model?.didAddItem = { [weak self] item in
            self?.modelDidAddItem(item: item)
        }
        self.model?.didRemoveItem = { [weak self] item in
            self?.modelDidRemoveItem(item: item)
        }
        self.model?.didChangeItem = { [weak self] item in
            self?.modelDidChangeItem(item: item)
        }
    }
    
    func itemModel(at index: Int) -> ExpirationItemCellViewModelType? {
        guard items.count > index else { return nil }
        
        return ExpirationItemCellViewModel(item: items[index], index: index)
    }
    
    func load() {
        guard let model = self.model else { return }
        
        load(model: model)
    }
    
    func showDetail(forItemAt index: Int) {
        guard items.count > index else { return }
        
        let detailViewModel = ItemDetailViewModel(itemsProvider: self)
        detailViewModel.expirationItem = items[index]
        
        delegate?.viewModel(self, wantsShowDetailFor: detailViewModel)
    }
    
    func showSettings() {
        delegate?.viewModelWantsShowSettings(self)
    }
    
    func showUserAccountSettings() {
        delegate?.viewModelWantsShowUserAccountSettings(self)
    }
    
    func addNewItem() {
        delegate?.viewModelWantsAddNewItem(self)
    }
    
    fileprivate func modelDidAddItem(item: ExpirationItem) {
        self.items.append(item)
        self.sortItems()
        
        if self.contentType.value == .empty {
            self.contentType.value = .itemsList
        }
        if let idx = self.items.firstIndex(where: { $0.firebaseId == item.firebaseId }) {
            self.didAddItem?(idx)
            delegate?.viewModel(self, didUpdateItems: items)
        }
    }
    
    fileprivate func modelDidRemoveItem(item: ExpirationItem) {
        if let idx = self.items.firstIndex(where: { $0.firebaseId == item.firebaseId }) {
            self.items.remove(at: idx)
            
            if self.contentType.value == .itemsList && self.items.isEmpty {
                self.contentType.value = .empty
            }
            self.didRemoveItem?(idx)
            delegate?.viewModel(self, didUpdateItems: items)
        }
    }
    
    fileprivate func modelDidChangeItem(item: ExpirationItem) {
        if let idx = self.items.firstIndex(where: { $0.firebaseId == item.firebaseId }) {
            self.items[idx] = item
            self.sortItems()
            self.didChangeAllItems?()
            delegate?.viewModel(self, didUpdateItems: items)
        }
    }
    
    fileprivate func load(model: UserExpirationItemsModelType) {
        contentType.value = .loading
        
        model.load { [weak self] items, error in
            self?.modelDidLoad(items: items, error: error)
        }
    }
    
    fileprivate func modelDidLoad(items: [ExpirationItem]?, error: Error?) {
        if let items = items {
            self.items = items
            sortItems()
        }
        else {
            self.items.removeAll()
            self.loadingError.value = error
        }
        self.didChangeAllItems?()
        self.contentType.value = self.items.isEmpty ? .empty : .itemsList
        delegate?.viewModel(self, didUpdateItems: self.items)
    }
    
    fileprivate func sortItems() {
        items.sort(by: isItemBigger(_:then:))
    }
    
    fileprivate func checkNotificationsNeeded() {
        for item in items {
            for reminder in item.reminders ?? [] {
                if reminder > 0 {
                    let newVal = true
                    
                    if needsNotificationsEnabled.value != newVal {
                        decideWhichNotificationPromptsIsNeeded()
                    }
                    needsNotificationsEnabled.value = newVal
                    
                    return
                }
            }
        }
        needsNotificationsEnabled.value = false
    }
    
    fileprivate func decideWhichNotificationPromptsIsNeeded() {
        guard let notifSettings = NotificationsManager.shared.currentNotificationSettings else {
            return
        }
        
        if notifSettings.authorizationStatus == .notDetermined {
            delegate?.viewModel(self, wantsShowNotificationsEnablingPrompt: false)
        }
        else if notifSettings.authorizationStatus == .denied {
            delegate?.viewModel(self, wantsShowNotificationsEnablingPrompt: true)
        }
    }
    
    fileprivate func isItemBigger(_ item1: ExpirationItem, then item2: ExpirationItem) -> Bool {
        return item1.expirationDate.timeIntervalSince(item2.expirationDate) < 0
    }
}

extension UserItemsViewModel: ExpirationItemsProvider {
    var expirationItems: [ExpirationItem]? {
        guard contentType.value == .itemsList else { return nil }
        return items
    }
}

class UserItemsLimitedViewModel: UserItemsViewModel, LimitedUserItemsViewModelType {
    
    var limitDelegate: LimitedUserItemsViewModelInteractionDelegate?
    
    var itemsLimit: Int?
    
    var hasItemsOverLimit = Dynamic(false)
        
    init(model: UserExpirationItemsModelType?, limit: Int?) {
        itemsLimit = limit
        super.init(model: model)
    }
    
    func updateLimit(limit: Int?) {
        itemsLimit = limit
        refreshHasItemsOverLimit()
    }
    
    func askToUpdateLimit() {
        limitDelegate?.viewModelWantsToIncreaseItemsLimit(self)
    }
    
    override var itemsCount: Int { min(super.itemsCount, itemsLimit ?? Int.max) }
    
    override func modelDidLoad(items: [ExpirationItem]?, error: Error?) {
        super.modelDidLoad(items: items, error: error)
        refreshHasItemsOverLimit()
    }
    
    override func modelDidAddItem(item: ExpirationItem) {
        super.modelDidAddItem(item: item)
        refreshHasItemsOverLimit()
    }
    
    override func modelDidRemoveItem(item: ExpirationItem) {
        super.modelDidRemoveItem(item: item)
        refreshHasItemsOverLimit()
    }
    
    private func refreshHasItemsOverLimit() {
        hasItemsOverLimit.value = self.itemsCount < super.itemsCount
    }
}
