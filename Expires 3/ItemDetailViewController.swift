//
//  ItemDetailViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import Firebase
import KDCircularProgress


protocol ItemDetailViewControllerDelegate: class {
    func controllerDidDeleteItem(_ controller: ItemDetailViewController)
}

class ItemDetailViewController: EditExpirationItemBaseViewController, ItemSummaryView {
    
    @IBOutlet weak var tableContainerView: UIView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var dayNumberLbl: UILabel?
    @IBOutlet weak var dayTextLbl: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var notesLabel: UILabel?
    @IBOutlet weak var progressBar: KDCircularProgress?

    
    var viewModel: EditExpirationItemViewModel! {
        didSet {
            viewModel?.delegate = self
        }
    }
    
    weak var delegate: ItemDetailViewControllerDelegate?
    
    private var deleteSectionIdx: Int {
        return super.numberOfSections(in: tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorInset = UIEdgeInsets.zero
        
        title = NSLocalizedString("Detail", comment: "")
        
        refreshUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        makeNavigationBarTransparent()
        
        navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        navigationItem.leftItemsSupplementBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .itemDetailScreenShown)
        
        //viewModel.test_PostNotification()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return super.numberOfSections(in: tableView) + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == deleteSectionIdx {
            return viewModel.canDeleteItem.value ? 1 : 0
        }
        
        let superNumberOfRows = super.tableView(tableView, numberOfRowsInSection: section)
        return viewModel.isEmpty.value ? 0 : superNumberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.section != deleteSectionIdx else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "Action Cell") as! ItemDetailActionCell
            cell.textLabel?.text = NSLocalizedString("Delete event", comment: "")
            cell.accessoryType = .none
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.backgroundColor = UIColor.expLightViolet.withAlphaComponent(0.1)
        
        if let iconTextCell = cell as? IconTextCell {
            iconTextCell.titleLabel.textColor = UIColor.expLightViolet
            
            if indexPath.row == dateRowIdx {
                iconTextCell.iconView.image = UIImage(named: "icon_expiration_date_violet")
            }
            else if indexPath.row == alertsRowIdx {
                iconTextCell.iconView.image = UIImage(named: "icon_advance_notice_violet")
            }
        }
        if let datePickerCell = cell as? DatePickerCell {
            datePickerCell.pickerTextColor = UIColor.expLightViolet
        }
        if let notesCell = cell as? IconTextViewCell {
            notesCell.textView.textColor = UIColor.expLightViolet
            notesCell.textViewPlaceholderLabel.textColor = UIColor.expLightViolet
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section != deleteSectionIdx else {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) in
                tableView.deselectRow(at: indexPath, animated: true)
            }))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (action) in
                tableView.deselectRow(at: indexPath, animated: true)
                
                self.viewModel.deleteItem()
            }))
            
            if let cell = tableView.cellForRow(at: indexPath) {
                alertController.popoverPresentationController?.sourceView = cell
                let rect = cell.bounds.insetBy(dx: cell.frame.midX - 10, dy: 20)
                alertController.popoverPresentationController?.sourceRect = rect
            } else {
                alertController.popoverPresentationController?.sourceView = view
                alertController.popoverPresentationController?.sourceRect = CGRect(origin: CGPoint(x: view.frame.midX, y: view.frame.maxY - 10), size: CGSize(width: 100, height: 44))
            }
            
            present(alertController, animated: true, completion: nil)
            
            return
        }
        
        super.tableView(tableView, didSelectRowAt: indexPath)
    }
    
    // MARK: Internal
    
    override func didChangeDate(date: Date) {
        super.didChangeDate(date: date)
        
        viewModel.expirationDate = date
        viewModel.updateItem()
    }
    
    override func didChangeReminderTimes(times: [Int]) {
        super.didChangeReminderTimes(times: times)
        
        viewModel.reminderIntervals = times
        viewModel.updateItem()
    }
    
    override func didChangeType(type: String) {
        super.didChangeType(type: type)
        
        viewModel.itemType = type
        viewModel.updateItem()
    }
    
    override func didChangeNotes(notes: String?) {
        super.didChangeNotes(notes: notes)
        
        viewModel.notes = notes
        viewModel.updateItem()
    }
    
    // MARK: Private
    
    private func refreshUI(animated: Bool = false) {
        
        itemType = viewModel.itemType
        itemTypeTextField.text = itemType
        
        expirationDate = viewModel.expirationDate
        reminderIntervals = viewModel.reminderIntervals
        notes = viewModel.notes
        
        tableContainerView.isHidden = viewModel.isEmpty.value
        
        tableView.reloadData()
        
        if let item = viewModel.expirationItem {
            configure(withItem: item, animated: animated)
            itemTypeTextField.textColor = titleColor(forStyle: style(forItem: item))
        }
    }
}
extension ItemDetailViewController: EditExpirationItemViewModelDelegate {
    func modelDidDeleteItem(_ model: EditExpirationItemViewModel) {
        self.delegate?.controllerDidDeleteItem(self)
        refreshUI(animated: true)
    }
    
    func modelDidUpdateItem(_ model: EditExpirationItemViewModel) {
        refreshUI(animated: true)
    }
}

extension ItemDetailViewController: ItemDetailViewControllerDelegate {
    func controllerDidDeleteItem(_ controller: ItemDetailViewController) {
        // cannot use self.navigationController, its different navigation stack
        if let navController = splitViewController?.viewControllers.first as? UINavigationController {
            navController.popViewController(animated: true)
        }
    }
}
