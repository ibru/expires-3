//
//  ReminderTimeViewController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/02/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import UserNotifications


protocol ReminderTimeViewControllerDelegate {
    func controller(_ controller: ReminderTimeViewController, didSetReminderTime reminderTime: Int)
}


class ReminderTimeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    enum Style {
        case itemDetail
        case addWizard
        
        var useBlurredBg: Bool {
            switch self {
            case .itemDetail: return false
            case .addWizard: return true
            }
        }
    }
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    
    let datePeriods = DatePeriods()
    
    var delegate: ReminderTimeViewControllerDelegate?
    
    var style: Style = .itemDetail
    
    var selectedReminderTimes: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("Advance notice", comment: "")
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor.expViolet
        
        view.backgroundColor = style == .itemDetail ? UIColor.expViolet2 : UIColor.clear
        tableView.backgroundColor = style == .itemDetail ? UIColor.expWhite : UIColor.clear
        backgroundImageView.isHidden = style == .itemDetail ? false : true
        blurEffectView.isHidden = style == .itemDetail ? true : false
        
        if style == .addWizard {
            tableView.separatorEffect = UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        appAnalytics.log(event: .reminderTimeScreenShown)
    }

   // MARK: UITableViewDataSource + UITableViewDelegate

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datePeriods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Period Cell", for: indexPath)
        
        cell.backgroundColor = UIColor.expLightViolet.withAlphaComponent(0.1)
        cell.textLabel?.text = datePeriods.localizedTitle(atIndex: indexPath.row)
        cell.textLabel?.textColor = .expViolet
        cell.textLabel?.backgroundColor = .clear
        cell.tintColor = .expViolet
        
        datePeriods.period(atIndex: indexPath.row).map {
            if selectedReminderTimes?.contains($0) ?? false {
                cell.accessoryType = .checkmark
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let notifSettings = NotificationsManager.shared.currentNotificationSettings {
            if notifSettings.authorizationStatus == .notDetermined {
                let viewController = AlertPresenter.alertStoryboard.instantiateViewController(withIdentifier: "AlertNotificationsAuthorizationViewController") as! AlertNotificationsAuthorizationViewController
                viewController.alertTitle = NSLocalizedString("Notifications not enabled", comment: "")
                viewController.alertSubtitle = NSLocalizedString("To get expiration reminders, you will need to enable notifications.", comment: "")
                viewController.actionButtonTitle = NSLocalizedString("Enable notifications", comment: "")
                viewController.doActionHandler = { completion in
                    appAnalytics.log(event: .reminderTimeScreenNotifSettingsAlertDoActionTouched)
                    
                    NotificationsManager.shared.requestNotificationAuthorization { (granted, error) in
                        NotificationsManager.shared.getCurrentNotificationSettings()
                        
                        completion(error == nil)
                        
                        if granted, let period = self.datePeriods.period(atIndex: indexPath.row) {
                            self.delegate?.controller(self, didSetReminderTime: period)
                            
                            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                viewController.notNowHandler = { completion in
                    appAnalytics.log(event: .reminderTimeScreenNotifSettingsAlertNotNowTouched)
                    completion(true)
                    self.navigationController?.popViewController(animated: true)
                }
                
                appAnalytics.log(event: .reminderTimeScreenNotifSettingsNotDeterminedAlertShown)
                AlertPresenter.shared.present(alertController: viewController)
                
                return
            }
            else if notifSettings.authorizationStatus == .denied {
                // ask to go to settings
                let viewController = AlertPresenter.alertStoryboard.instantiateViewController(withIdentifier: "AlertNotificationsAuthorizationViewController") as! AlertNotificationsAuthorizationViewController
                viewController.alertTitle = NSLocalizedString("Notifications are disabled", comment: "")
                viewController.alertSubtitle = NSLocalizedString("To get expiration reminders, you will need to enable notifications in system Settings", comment: "")
                viewController.actionButtonTitle = NSLocalizedString("Go to Settings", comment: "")
                viewController.doActionHandler = { completion in
                    appAnalytics.log(event: .reminderTimeScreenNotifSettingsAlertDoActionTouched)
                    
                    completion(true)
                    
                    if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                        UIApplication.shared.open(settingsUrl)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                viewController.notNowHandler = { completion in
                    appAnalytics.log(event: .reminderTimeScreenNotifSettingsAlertNotNowTouched)
                    completion(true)
                    self.navigationController?.popViewController(animated: true)
                }
                
                appAnalytics.log(event: .reminderTimeScreenNotifSettingsDeniedAlertShown)
                AlertPresenter.shared.present(alertController: viewController)
                
                return
            }
        }
        
        if let period = datePeriods.period(atIndex: indexPath.row) {
            self.delegate?.controller(self, didSetReminderTime: period)
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            self.navigationController?.popViewController(animated: true)
        }
    
    }
    
}


