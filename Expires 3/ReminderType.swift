//
//  ReminderType.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 10/02/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit



struct ReminderType {
    private struct DictKey {
        static let title = "title"
        static let order = "order"
    }
    
    let id: String
    let title: String
    let order: Int
    
    init?(id: String, dictionary: JSONDict) {
        guard let title = dictionary[DictKey.title] as? String, let order = dictionary[DictKey.order] as? Int else {
            return nil
        }
        self.id = id
        self.title = title
        self.order = order
    }
    
    init(id: String? = nil, title: String, order: Int) {
        self.id = id ?? "_unknown"
        self.title = title
        self.order = order
    }
    
    var dictValue: JSONDict {
        return [DictKey.title: title as Any, DictKey.order: order as Any]
    }
    
}

