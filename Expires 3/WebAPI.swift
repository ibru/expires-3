//
//  WebAPI.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 10/02/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation
import FirebaseDatabase

public enum APIEndpoint {
    case items(userId: String)
    case publicTypes
    case privateTypes(userId: String)
    
    var urlPath: String {
        switch self {
        case .items(let userId): return "/private/" + userId + "/items"
        case .publicTypes: return "/public/types"
        case .privateTypes(let userId): return "/private/" + userId + "/types"
        }
    }
}

extension DatabaseReference {
    func child(_ endpoint: APIEndpoint) -> DatabaseReference {
        return child(endpoint.urlPath)
    }
}

