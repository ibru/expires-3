//
//  IAPTransactionsController.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 02/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import StoreKit


protocol IAPProductContentDeliverable {
    func deliverContent(forProduct productId: String, transaction: SKPaymentTransaction, completion: (_ wasDelivered: Bool) -> Void)
}
struct NoActionProductContentDeliverer: IAPProductContentDeliverable {
    func deliverContent(forProduct productId: String, transaction: SKPaymentTransaction, completion: (_ wasDelivered: Bool) -> Void) {
        completion(true)
    }
}

protocol IAPTransactionsControllerDelegate: class {
    func controller(_ controller: IAPTransactionsController, didStartPurchaing productId: String)
    func controller(_ controller: IAPTransactionsController, didCompletePurchasing productId: String, with transaction: SKPaymentTransaction)
    func controller(_ controller: IAPTransactionsController, didRestore transactions: [SKPaymentTransaction])
    func controller(_ controller: IAPTransactionsController, didReceiveError error: Error?)
}

class IAPTransactionsController: NSObject {
    
    fileprivate let productDeliverer: IAPProductContentDeliverable
    
    weak var delegate: IAPTransactionsControllerDelegate?
    
    init(deliverer: IAPProductContentDeliverable = NoActionProductContentDeliverer()) {
        self.productDeliverer = deliverer
    }
    
    func startObservingTransactions() {
        SKPaymentQueue.default().add(self)
    }
    
    func stopObservingTransactions() {
        SKPaymentQueue.default().remove(self)
    }
}

extension IAPTransactionsController: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            let productId = transaction.payment.productIdentifier
            
            switch transaction.transactionState {
            case .purchasing:
                delegate?.controller(self, didStartPurchaing: productId)
                
            case .purchased:
                productDeliverer.deliverContent(forProduct: productId, transaction: transaction) { delivered in
                    if delivered {
                        SKPaymentQueue.default().finishTransaction(transaction)
                        delegate?.controller(self, didCompletePurchasing: productId, with: transaction)
                        NotificationCenter.default.post(name: .IAPProductWasPurchased, object: self, userInfo: ["productId": productId])
                    } else {
                        delegate?.controller(self, didReceiveError: nil)
                    }
                }
                
            case .failed:
                SKPaymentQueue.default().finishTransaction(transaction)
                delegate?.controller(self, didReceiveError: transaction.error)
                
            case .restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                delegate?.controller(self, didRestore: [transaction])
                NotificationCenter.default.post(name: .IAPProductWasPurchased, object: self, userInfo: ["productId": productId])
                
            case .deferred:
                break
            }
        }
        
        // TODO: post notification with all purchased / restored products?
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        delegate?.controller(self, didReceiveError: error)
    }
    
    
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        // handled separately by paymentQueue(:, updatedTransactions:) with .restored case
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return true
    }
}

extension Notification.Name {
    static let IAPProductWasPurchased = Notification.Name("IAPProductWasPurchased")
}

