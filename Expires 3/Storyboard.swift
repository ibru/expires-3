//
//  Storyboard.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 03/01/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import UIKit

struct Storyboard {
    static var main: UIStoryboard { return UIStoryboard(name: "Main", bundle: nil) }
}
