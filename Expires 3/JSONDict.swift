//
//  JSONDict.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 08/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation

public typealias JSONDict = [String: Any]
