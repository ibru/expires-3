//
//  IconTextFieldCell.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 27/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class IconTextViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewPlaceholderLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.layoutMargins = UIEdgeInsets(top: 10, left: 30, bottom: 8, right: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
