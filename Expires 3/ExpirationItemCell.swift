//
//  ExpirationItemCell.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit
import KDCircularProgress

let highlightedDaysRemaining = 14


class ExpirationItemCell: UICollectionViewCell, ItemSummaryView {
    
    private var selectedBGColor = UIColor.rgb(200, 200, 200)
    private var highlightedBGColor = UIColor.rgb(200, 200, 200)
    private var bgColor = UIColor.white
    
    enum ColorStyle {
        case violet
        case red
    }
    
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var dayNumberLbl: UILabel?
    @IBOutlet weak var dayTextLbl: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var notesLabel: UILabel?
    @IBOutlet weak var progressBar: KDCircularProgress?
    
    @IBOutlet weak var notesWidth: NSLayoutConstraint!
    
    
    var bgAlpha: CGFloat! {
        didSet {
            contentView.alpha = bgAlpha
        }
    }
    
    var isNotesHiden: Bool! {
        didSet {
            notesLabel?.isHidden = isNotesHiden
            notesWidth?.constant = (isNotesHiden! ? 0 : 150)
        }
    }
    
    private static let minAlpha: CGFloat = 0.4
    
    static func alphaForCell(atIndex idx: Int) -> CGFloat {
        let cellAlpha: CGFloat = 1.0 - CGFloat(idx) * 0.15
        
        return cellAlpha >= minAlpha ? cellAlpha : minAlpha
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        contentView.layer.cornerRadius = 5
        
        applyStyle(style: .violet)
        bgAlpha = 1
        isNotesHiden = true
        
        notesLabel?.textColor = UIColor.expLightViolet
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override var isSelected: Bool {
        didSet {
            contentView.backgroundColor = isSelected ? selectedBGColor : bgColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            contentView.backgroundColor = isHighlighted ? highlightedBGColor : bgColor
        }
    }
    
    func configureWith(model viewModel: ExpirationItemCellViewModelType) {
        configureWith(viewModel.item, forIndexPath: IndexPath(item: viewModel.index, section: 0))
    }

    @available(*, deprecated: 3.0)
    func configureWith(_ item: ExpirationItem, forIndexPath indexPath: IndexPath) {
        bgAlpha = ExpirationItemCell.alphaForCell(atIndex: indexPath.row)
        
        configure(withItem: item)
    }
    
    
    func highlightProgress(forItem item: ExpirationItem) {
        
        doOnMainThreadAfter(delay: 0.3) {
            self.updateProgress(progress: self.progress(forItem: item), animated: true)
        }
    }
    
}
