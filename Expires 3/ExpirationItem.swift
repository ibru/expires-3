//
//  ExpirationItem.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 17/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

public struct ExpirationItem {
    
    private struct DictKey {
        static let title = "title"
        static let expirationDate = "expirationDate"
        static let reminders = "reminders"
        static let notes = "notes"
    }
    
    static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        return dateFormatter
    }()
    
    static var unsavedItemId: String { return "_l"+NSUUID().uuidString }
    
    let firebaseId: String
    var reminders: [Int]?
    var expirationDate: Date
    var title: String
    var notes: String? = nil
    //let locationLat: Double?
    //let locationLon: Double?

    public init(id: String, title: String, date: Date, reminders: [Int]? = nil, notes: String? = nil) {
        self.firebaseId = id
        self.title = title
        self.expirationDate = date
        self.reminders = reminders
        self.notes = notes
    }
    
    public init?(id: String, dictionary: JSONDict) {
        guard let title = dictionary[DictKey.title] as? String else { return nil }
        
        var aDate = dictionary[DictKey.expirationDate] as? Date
        if aDate == nil, let dateStr = dictionary[DictKey.expirationDate] as? String {
             aDate = ExpirationItem.dateFormatter.date(from: dateStr)
        }
        guard let date = aDate else { return nil }
        
        self.firebaseId = id
        self.title = title
        self.expirationDate = date
        self.reminders = dictionary[DictKey.reminders] as? [Int]
        self.notes = dictionary[DictKey.notes] as? String
    }
    
    var dictValue: JSONDict {
        var dict: JSONDict = [DictKey.title: title as Any, DictKey.expirationDate: ExpirationItem.dateFormatter.string(from: expirationDate) as AnyObject]
        
        if let reminders = reminders {
            dict[DictKey.reminders] = reminders as Any
        }
        if let notes = notes {
            dict[DictKey.notes] = notes as Any
        }
        return dict
    }

}

extension ExpirationItem: Equatable {
    public static func == (lhs: ExpirationItem, rhs: ExpirationItem) -> Bool {
        return lhs.firebaseId == rhs.firebaseId && lhs.title == rhs.title && lhs.expirationDate == rhs.expirationDate
    }
}
