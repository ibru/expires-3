//
//  Constants.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 21/04/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import Foundation

struct K {
    static let itemsLimitCount = 5
    
    static let feedbackEmailSubject = "My feelings about Expires app"
    static let feedbackEmailAddress = "jiri.urbasek@gmail.com"
}

