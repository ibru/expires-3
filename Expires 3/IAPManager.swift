//
//  IAPManager.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 01/03/2018.
//  Copyright © 2018 Jiri Urbasek. All rights reserved.
//

import Foundation
import StoreKit
import TPInAppReceipt

protocol IAPManagerDelegate {
    func managerNoProductsAvailableToBuy(manager: IAPManager)
    func manager(_ manager: IAPManager, didReceiveResponseForProduct availableProduct: String, product: SKProduct)
}

public enum IAPProduct: String {
    case expiresPro = "com.jiriurbasek.expires3.iap.expires_pro"
}

public class IAPManager: NSObject {
    
    var delegate: IAPManagerDelegate?
    
    func requestProductData(productIdentifiers: Set<String>) throws {
        if SKPaymentQueue.canMakePayments() {
            let request = SKProductsRequest(productIdentifiers:productIdentifiers)
            request.delegate = self
            request.start()
        }
        else {
            let error = NSError(domain: "IAP", code: 1, userInfo: [NSLocalizedDescriptionKey: "In-App Purchases Not Enabled"])
            throw error
        }
    }
    
    func buyProduct(product: SKProduct) {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func restoreTransactions() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

extension IAPManager {
    class func localyValidatedReceiptProducts() -> [String]? {
        do {
            let receipt = try InAppReceipt.localReceipt()
            try receipt.verify()
            
            let productIDs = receipt.purchases.map { $0.productIdentifier }
            return productIDs
        } catch {
            return nil
        }
    }
    
    class func remotelyValidateReceiptProducts(completion: @escaping (_ productIDs: [String]?, _ error: Error?) -> ()) {
        
        var productIDs: [String]?
        var anError: Error?
        defer {
            DispatchQueue.main.async { completion(productIDs, anError) }
        }
        
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!) {
            var receiptData:NSData?
            do {
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            } catch {
                anError = error
                print("ERROR: " + error.localizedDescription)
                return
            }
            
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            let requestDictionary = ["receipt-data":base64encodedReceipt!]
            
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                // Apple recomends never call their url from device directly, use own server to mediate
                #if DEBUG
                let validationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"
                #else
                let validationURLString = "https://buy.itunes.apple.com/verifyReceipt"
                #endif
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                
                let queue = DispatchQueue(label: "itunesConnect")
                queue.async {
                    URLSession.shared.uploadTask(with: request, from: requestData) { (data, response, error) in
                        guard let data = data, error == nil else {
                            anError = error
                            print("the upload task returned an error: \(error ?? "couldn't upload" as! Error)")
                            return
                        }
                        do {
                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                            
                            // finish implementation
                            
                            productIDs = []
                            
                        } catch {
                            anError = error
                            print("json serialization failed with error: \(error)")
                        }
                    }.resume()
                }
            } catch {
                anError = error
                print("json serialization failed with error: \(error)")
            }
        }
    }
}

extension IAPManager: LocalIAPValidator {
    func localyValidatedReceiptProducts() -> Set<IAPProduct> {
        guard let productIDs = IAPManager.localyValidatedReceiptProducts() else { return [] }
        
        let products = productIDs.compactMap { IAPProduct(rawValue: $0) }
        return Set(products)
    }
}

extension IAPManager: RemoteIAPValidator {
    func validateReceipt(completion: ((Set<IAPProduct>) -> Void)?) {
        // we didnt implement yet proper remote receipt validation so use local receipt validation instead
        let products = localyValidatedReceiptProducts()
        completion?(products)
    }
    
    
}

extension IAPManager: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        if response.products.count == 0 {
            delegate?.managerNoProductsAvailableToBuy(manager: self)
        }
        for product in response.products {
            delegate?.manager(self, didReceiveResponseForProduct: product.productIdentifier, product: product)
        }
    }
}
