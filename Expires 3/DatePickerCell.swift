//
//  DatePickerCell.swift
//  Expires 3
//
//  Created by Jiri Urbasek on 27/01/2017.
//  Copyright © 2017 Jiri Urbasek. All rights reserved.
//

import UIKit

class DatePickerCell: UITableViewCell {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    var pickerTextColor: UIColor? {
        didSet {
            if let color = pickerTextColor {
                do {
                    try ObjC.catchException {
                        self.datePicker.setValue(color, forKey: "textColor")
                        self.datePicker.perform(Selector(("setHighlightsToday:")), with: false)
                    }
                }
                catch {
                    // the text color will be just black
                }
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.layoutMargins = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
        
        // make sure didSet handler is called
        let color = pickerTextColor
        pickerTextColor = color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
